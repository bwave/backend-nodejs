var express = require('express');
var app = express();
var bodyParser = require('body-parser');
//var passport = require('passport');
var flash    = require('connect-flash');
//var db = require('./config/database.js');
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var session      = require('express-session');

var cors = require('cors');
var port = 8001;
app.use(cors());
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser());
app.use(function (req, res, next) {

    res.setHeader( 'Access-Control-Allow-Origin', '*');
    res.setHeader( 'Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    res.setHeader( 'Access-Control-Allow-Headers', 'X-Requested-With, X-AUTHENTICATION, X-IP, Content-Type, Accept, Authorization, B-Token, content-type');
    res.setHeader( 'Access-Control-Allow-Credentials', true);

    next();
});
app.set('view engine', 'ejs'); // set up ejs for templating
app.use(session({secret: 'abcd' })); // session secret
app.use(flash()); // use connect-flash for flash messages stored in session

var http = require('http').Server(app);
var io = require('socket.io')(http);
//var my = require('./socketbox.js')(io, myEmitter);

//console.log(my);
//my.webs.parsJson(null,null);

//exports.io = io;
//exports.myEmitter = myEmitter;
// routes ======================================================================
require('./app/routes.js')(app, io);//, passport); // load our routes and pass in our app and fully configured passport
/*
io.on('connection', function (socket)
      {
      socket.auth = false;
      console.log('connection');
      socket.on('authenticate', function(data)
            {
            //check the auth data sent by the client
            this.checkAuthToken(data.login, data.password, socket);
            console.log(sockettable);
            socket.on('CH01', function (from, msg)
                  {
                      console.log('MSG', from, ' saying ', msg);
                  });
            socket.once('disconnect',function()
                     {
                    this.updateBoxInfo(socket.handshake.address, bo_id, false);
                    });
            setTimeout(function(){
                //If the socket didn't authenticate, disconnect it
                if (!socket.auth) {
                console.log("Disconnecting socket ", socket.id);
                socket.disconnect('unauthorized');
                }
            }, 1000);
            });
      });
*/
app.listen(process.env.PORT || 5000, function()
    {
        console.log("Serveur on port 5000");
    });

http.listen(3000, function () {
    console.log('Socket on *:3000');
});
