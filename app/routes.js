// app/routes.js
var sock = require('../socketbox.js');
//var User       = require('./models/user');
//var configAuth = require('../config/auth');
var bodyParser = require('body-parser');
var configAuth = require('../config/auth.js');
var sqlconnection = require('./database.js');
//var myEmitter = require('../socketbox.js').myEmitter;
module.exports = function (app, io) {
    app.use(bodyParser());
    var jwt = require('express-jwt');
    // Create an eventEmitter object
    var events = require('events');
    var myEmitter = new events.EventEmitter();
    // Emiter listener is added after ecallback function, in module route, and in box
    app.use(bodyParser.json({ type: 'application/*+json' }))
    var urlencodedParser = bodyParser.urlencoded({ extended: false });
    var jsonParser = bodyParser.json();
    var debug = require('debug')('routes');
    var command = require('./commands.js');
    var module = require('./modules.js');
    var async = require('async');
    var box = require('./boxs.js');
    var user = require('./user.js');
    var Room = require('./room');

    var socketbox = require('../socketbox');
    socketbox.io = io;
    console.log("myemitter : ", myEmitter)
    socketbox.myEmitter = myEmitter;
    socketbox.init();
    funki = require('../socketbox').variable;
    // this might be in socketbox
    io.on('connection', function (socket)
	  {
      	  socket.auth = false;
	      console.log('connection');
	      socket.on('authenticate', function(data)
			{
			    //check the auth data sent by the client
			    socketbox.checkAuthToken(data.login, data.password, socket);
			    console.log(socketbox.sockettable);
			    socket.on('event', function (msg)
				      {
/*				      	console.log("command === : ", msg.command);
				      	console.log("module:  ", msg.modules);
				      	console.log("command.instance:  ", msg.command.instance);*/
				      	socketbox.db_savemodulestate(socket.ba_id, msg.modules, msg.command.name, msg.command.id, msg.command.instance, msg.command.level, msg.command.scale)
					  	console.log('saying ', msg);
				      });
			    socket.once('disconnect',function()
					{
					    socketbox.updateBoxInfo(socket.handshake.address, socketbox.bo_id, false);
					});
			    setTimeout(function(){
				//If the socket didn't authenticate, disconnect it
				if (!socket.auth) {
               			    console.log("Disconnecting socket ", socket.id);
            			    socket.disconnect('unauthorized');
        			}
    			    }, 1000);
			});
	  });
    /*
    ** Next is used to login user
    **
    */
    function output(req, res)
    {
   	console.log("Output show ()");
  	res.send(200, "YOLO");
    };
    myEmitter.addListener('output', output);

    app.get("/test", function(req, res)
	    {
		//	socketbox.myEmitter = myEmitter;
	  	//  socketbox.init();
		console.log("in test : ");
		myEmitter.emit("hello", req,res);
	    });

    app.get("/output", function(req, res)
	    {
    		myEmitter.once("output", output);
		console.log("in output");

	    });
    var authCheck = jwt(
	{
	    secret: new Buffer('jsgZuKD2ucKSGKMDsWBRASB6WCSZkVD4_2wy9HIbcxvA6T7wNMbVTn-7mJ1mmwN5', 'base64'),
	    audience: 'FnXpSGTabzruAjIWOlg0pwc6EFk7xdU7'
	});

    function callbackAsyncGetModule(sender, err)	
    {
	console.log("MODULE CALLBACK SENDER : ", sender, "  ee:", err);
	
	if (err)
	{
	    console.log(err);
	    myEmitter.emit('e-callbackfinal', 400, "unable to fetch module");
	    return;
	}
	else
	    switch (sender)
	{
	    case 'module':
	    myEmitter.emit('e-callback', "module", "Module successfully fetched", null);
	    break;
	    case 'switch':
	    myEmitter.emit('e-callback', "switch", "Switch successfully fetched", null);
	    break;
	    case 'sensor':
	    myEmitter.emit('e-callback', "sensor", "Sensor successfully fetched", null);
	    break;
	}
    }
    // Next is a array of listed event who need to be called. At every call, use shift() to delete the first element
    function eventCrossroad(origin, sender, next, code, message)
    {
	if (origin == "r_getallmodules")
	{
	    debug("In first if : ", code, " ,", origin, " :(-, ", sender, "next : ", next, "message : ", message);
	    if (code == 200)
		myEmitter.emit("e-callback", "box", message, null);
	    else
		myEmitter.emit("e-callback", "box", null, message);
	    return;
	}
	if (next == null || next.length == 0)
	{
	    console.log("No more function need to be call, callback origin : ", origin);
	    debug("sender : ", sender, " | code :", code, " | message : ", message);
	    if (origin == null)
		return console.log("Error, origin can't be null, caller : ", sender);
	    myEmitter.emit(origin, code, message); // we are going back to our route, we dont need sender and next anymore
	    return;
	}
	else if (next.length > 0)
	{
	    debug(next);
	    console.log("Calling next function with event : ", next[0], " | from :", sender);
	    debug("Origin : ", origin, " | sender : ", sender, " | next : ", next, " | code :", code, " | message : ", message);
	    myEmitter.emit(origin, sender, next[0], code, message);
	}
    }
    // Those 3 variables are used only in ecallback listener
    var switchresult = false;
    var sensorresult = false;
    var moduleresult = false;

    // Maybe this function should go in another files ?
    var ecallback = function(sender, message, err)
    {
	switch (sender)
	{
	    case "box":
	    command.command_list  = [];
	    module.module_list = [];
	    // GET Module list
	    console.log("Calling getmodule => ", sender);
	    async.eachSeries(box.boxid, module.getmodule,callbackAsyncGetModule.bind(callbackAsyncGetModule, "module"));
	    break;
	    case "module":
	    moduleresult = true;
	    console.log("Module request executed successfully");
	    // Get Switch command
	    console.log("Calling getswitcommand", module.module_list[0]);
	    async.eachSeries(module.module_list[0],command.getswitchcommand, callbackAsyncGetModule.bind(callbackAsyncGetModule, "switch"));
	    // Get SensorrMultiLevel command
	    console.log("Calling getssensorcommand");
	    async.eachSeries(module.module_list[0],command.getsensormlvl, callbackAsyncGetModule.bind(callbackAsyncGetModule, "sensor"));
	    break;
	    case "switch":
	    switchresult = true;
	    if (moduleresult && switchresult && sensorresult)
	    {
		console.log("allgood switch");
		if (err)
		{
		    console.log("switch failure");
		    myEmitter.emit('r_getallmodules',400, "failed"); 
		}
		else
		{
		    console.log("switch failure");
		    myEmitter.emit('r_getallmodules', 200);
		}
	    }
	    break;
	    case "sensor":
	    sensorresult = true;
	    if (moduleresult && switchresult && sensorresult)
	    {
		console.log("If allgood sensor");
		if (err)
		{
		    console.log("sensor failure");
		    myEmitter.emit('r_getallmodules',400, "failed"); 
		}
		else
		{
		    console.log("sensor succes");
		    myEmitter.emit('r_getallmodules', 200, "success");
		}
	    }
	    break;
	}
    }
    myEmitter.addListener('e-callback', ecallback);
    myEmitter.addListener('eventCrossroad', eventCrossroad);
    var tmpBox = require('./models/models').BoxApplication;
    app.put('/commands', urlencodedParser, authCheck, function(req, res)
	    {

		if (req.user)
		    var userid = getUserID(req.user);
		else{
		    res.send(400, "Unable to get userid");
		    return;
		}
		if (typeof(req.body["name"]) == "undefined" ||
		    typeof(req.body["id"]) == "undefined" ||
		    typeof(req.body["instance"]) == "undefined" ||
	    	    typeof(req.body["level"]) == "undefined") 
	    	{
	    	    console.log("UNDEFINED");
		    res.send(400, "Mandatory data not set, impossible to update command  - name - id - level - instance - [scale]");
		    return;
		}
		// FOr some obscur reason, eventEmitter is not fired in updatecommand if it's not in a sequelize promesses. No clue why.
		if (req.body["name"] != "Switch" && req.body["name"] != "Sensormultilevel")
		{
		    res.send(400, "No module found with this name : "+ req.body["name"], " or mandatory data not set - scale");
		    return;
		}
		if (req.body["name"] == "Sensormultilevel" && typeof(req.body["scale"]) == "undefined")
		{
		    res.send(400, "Mandatory data not set - scale");
		    return;
		}
	    	console.log("Updating command with paramater : ", req.body["id"], " - ",  req.body["level"], " - ", req.body["name"], "--", req.body["instance"])
		var sendme =
		    {
			"module" :
			{
			    "id": req.body["id"]
			},
			"command":
			{
			    "name": "SwitchBinary",
			    "instance": req.body["instance"],
			    "level": req.body["level"]
			}
		    }
		debug("Emiting to Command of benbox");
		console.log(sendme);
//		var ba_id = "hEaeRtgQrCAtsRlfDavn4SHR4DCSXHoW";//row[0].dataValues.b_fk_ba_id;
		tmpBox.findAll({include: [{ model: Box, require:true, where: {b_id: req.body["b_id"]}}]}).then(
		    function(row)
		    {
			debug("Inside tmpbox");
			if (row.length > 0)
			{
			    console.log("row : ", row[0].dataValues);
			    for (i in socketbox.sockettable)
			    {
				if (socketbox.sockettable[i].ba_id == row[0].dataValues.ba_id)
				{
				    console.log("Socket : ", socketbox.sockettable[i].ba_id);
				    console.log("--------------------------------------------------------------------------------------------------------------------------------------");
				    socketbox.sockettable[i].emit('Command', sendme);
				    res.send(200, "emited");
				}
			    }
			    /*									       for (i in socketbox.sockettable)
												       {
												       if (socketbox.sockettable[i].ba_id == ba_id)
												       {
												       debug("Socket finded : " + socketbox.sockettable[i].ba_id);
												       socketbox.sockettable[i].emit('Command', sendme);
												       res.send(200, "emited");
												       }
												       debug("In loop : ");
												       }
			    */
			}
			res.send(400,"error no socket founded");
		    }).error(function(err)
			     {
				 console.log("Error findbox", err);
				 res.send(400,"error switch ba");
			     });
		//		socket.emit('Command', sendme);
		//			res.send(200, "Command send");
		//command.updatecommand(myEmitter, 'r_coms', null, null, userid, req.body["name"], req.body["id"], req.body["level"], req.body["scale"]);
		//  		myEmitter.once('r_coms', function (errorcode, message)
		//			       {
		//				   res.send(errorcode, message);
		//			       });
	    });	
    app.get('/modules', urlencodedParser, authCheck, function(req, res, next)
	    {
		var url = require('url');
		var url_parts = url.parse(req.url, true);
		var query = url_parts.query;
		switchresult = false;
		sensorresult = false;
		moduleresult = false;
		box.boxid = -1;
//	  	var userid= "3118187645865548110712";
//	  	myEmitter.emit('test');
		
		  if (req.user)
		  var userid = getUserID(req.user);
		  else{
			  res.send(400, "not logged");
			return;
			}
				// Those variable are used as trigger, when all are true, event is rised and the result of command.command_list is send to the browser
		// They need to be set false every time this function is called
		debug("function get/allmodules");
		box.getboxid(myEmitter, "r_getallmodules", "getallmodules", null, null, userid, query["b_identifier"]);
    		myEmitter.once('r_getallmodules', function (errorcode, message)
			       {
				   if (errorcode != 200)
				       res.send(errorcode, message);
				   else
				       res.send(200, command.command_list);
			       });
	    }
	   );

    app.get('/boxs', urlencodedParser, authCheck, function(req, res, next)
	    {
		//		console.log("create user");
		//		user.createuser(myEmitter);//, "FnXpSGTabzruAjIWOlg0pwc6EFk7xdU7", "newuser@test.com", "sequelizeduser");
		//		finduser(query["clientID"]);
		//		res.send(200, "ok");
		if (req.user)
		    userid = getUserID(req.user);
		else
		    return;
		var url = require('url');
		var url_parts = url.parse(req.url, true);
		var query = url_parts.query;
		//		jwt.verify(req.headers.authorization)
		console.log("Looking for user's box, clientID is : ", userid);
		if (userid)
		    box.getuserbox(myEmitter, "r_getbox", null, null, null, userid);
		else
		    res.send(200, "clientID mandatory");
		myEmitter.once('r_getbox', function(code, message)
			       {
				   res.send(code, message);
			       });
	    }
	   );
    //Working, but should be replace by upsert
    app.post('/boxs', urlencodedParser, authCheck, function(req, res)
	     {
		 try
		 {
		     box_id = req.body["id"];
		     ub_userboxname = req.body["name"];
		     ub_userboxdescription = req.body["description"];
		     password = req.body["password"];
		 }
		 catch(err)
		 {
		     console.log("Postbox data undefined");
		     res.send(400, "Error receiving mandatory data");
		     return;
		 }
    		 if (req.user)
		     userid = getUserID(req.user);
		 else
		     res.send(401, "unable to verify token");
		 // this next line might be deleted
		 if (!box_id ||  !ub_userboxname || !password)
		 {
		     res.send(400, "Error receiving mandatory data");
		     return;
		 }
		 if(!ub_userboxdescription)
		     ub_userboxdescription = "No description";
		 // for test purpose only
		 console.log(" Adding box with id : ", box_id, " password : ", password);
		 box.addbox(myEmitter, "r_postbox", null, null, box_id, userid, ub_userboxname, ub_userboxdescription, password);
		 myEmitter.once("r_postbox", function(code, message)
				{
				    res.send(code, message);
				    return;
				});
	     }
	    );
//		var userid= "115526176632606751752";
    app.get('/rooms', urlencodedParser, authCheck, function(req, res)
	    {
		var url = require('url');
		var url_parts = url.parse(req.url, true);
		var query = url_parts.query;

		if (req.user)
		    userid = getUserID(req.user);
		else
		    res.send(401, "unable to verify token");
		console.log("============================================================================== req :" , req.body["b_id"], " <-------------------------", query["b_id"]);
		Room.getRoom(myEmitter, 'r_getroom', null, null, userid, query["b_id"]);
		myEmitter.once('r_getroom', function(code, message)
			       {
				   console.log("here ?: ", code, ' <- ' , message);
				   res.send(code, message);
			       });
	    });
    app.post('/rooms', urlencodedParser, authCheck, function(req, res)
    	     {
    		 if (req.user)
			     userid = getUserID(req.user);
			 else
			     res.send(401, "unable to verify token");
		 var id = null;
			 if (req.body["id"])
			     id = req.body["id"];
			 if (req.body["name"])
			     Room.createRoom(myEmitter, "r_postroom", null, null, userid, req.body["name"], id, req.body["b_id"]);
			 else
			     res.send(400, "mandatory data not set");
			 myEmitter.once('r_postroom', function(code, message)
					{
					    res.send(code, message);
					});
    	     });
    app.get('/rooms/modules', urlencodedParser, authCheck, function(req, res)
	    {
		var url = require('url');
		var url_parts = url.parse(req.url, true);
		var query = url_parts.query;
    		if (req.user)
		    userid = getUserID(req.user);
		else
		    res.send(401, "unable to verify token");

		if (!query["id"])
		    res.send(400, "mandatory data not set roomid");
		Room.getRoomModule(myEmitter, "r_getroommodule", null, null, userid, 3, query["id"], query["b_id"]);
		myEmitter.once('r_getroommodule', function(code, message)
			       {
				   res.send(code, message);
			       });
	    });
    app.post('/rooms/modules', urlencodedParser, authCheck, function(req, res)
	     {

    		 if (req.user)
		     userid = getUserID(req.user);
		 else
		     res.send(401, "unable to verify token");
		 if (!req.body["id"] || !req.body["id_module"])
		     res.send(400, "mandatory data not set id_module - roomid");
		 Room.updateRoomModule(myEmitter, "r_postroommodule", null, null, userid, req.body["roomid"], req.body["moduleid"], req.body["b_id"]);
		 myEmitter.once('r_postroommodule', function(code, message)
				{
				    res.send(code, message);
				});
	     });
    app.delete('/rooms/modules', urlencodedParser, authCheck, function(req, res)
	       {
    		   if (req.user)
		       userid = getUserID(req.user);
		   else
		       res.send(401, "unable to verify token");
		   if (!req.body["roomid"] || !req.body["moduleid"])
		       res.send(400, "mandatory data not set moduleid - roomid");
		   Room.deleteRoomModule(myEmitter, "r_deletemodule", null, null, userid, req.body["roomid"], req.body["moduleid"]);
		   myEmitter.once("r_deletemodule", function(code, message)
				  {
				      res.send(code, message);
				  });
	       });
    // route middleware to make sure a user is logged in
    function getUserID(userToken) 
    {
	user.getuser(myEmitter, userToken.sub.split('|')[1], "noneed", "name", null);
	debug("Getting userID from req.user : ", userToken, " userid : ", userToken.sub.split('|')[1]);
	return (userToken.sub.split('|')[1]);
    }
};

