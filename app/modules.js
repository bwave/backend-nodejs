var sqlconnection = require('./database.js');
var Module = require('./models/models.js').Module;
var debug = require('debug')('modules');

module.exports = {

	module_list: [],
	getmodule: function getmodule(query0, callback0)
	{
		Module.findAll({where: {m_fk_box_id: query0}}).then(function(rs_module)
		{
			debug("Selecting module with fk_box_id : %s", query0);
			var result = [];
			for (i in rs_module)
				result.push(rs_module[i].dataValues);
			if (rs_module.length > 0)
			{
				module.exports.module_list.push(result);								    
			}
			return callback0(null, result);
		}).error(function(err)
		{
			debug("EROOOOOOO");
			debug(err);
			return callback0(err, null);
		});
		
	},
	deletemodule: function deletemodule(mdelete)
	{
		Module.destroy({
			where: { m_moduleid: {not: mdelete} }
		}).then(function (rowdeleted)
		{
			if (rowdeleted > 0)
				console.log("Module deleted : ", rowdeleted);
			else
				console.log("no module deleted");
		}).error(function (errdelete)
		{
			console.log("Error deleting module");
			debug(errdelete);
		})
	},
	updatemodule: function updatemodule(ba_id, moduleid, name, description)
	{
		Box.findAll(
		{
			where: {b_fk_ba_id : ba_id}
		}).then(function (boxrow)
		{
			if (boxrow && boxrow.length > 0)
			{
				debug("box found :", boxrow);
				Module.upsert(
				{
					m_moduleid: moduleid, 
					m_modulename: name,
					m_description: description,
					m_fk_box_id: boxrow[0].dataValues.b_id
				}).then(function(rowmodule)
				{
					if (rowmodule > 0)
					{
						console.log("Module successfully inserted");
					}
					else
					{
						console.log("Fail to insert module");
					}
				}).error(function (errmodule)
				{
					console.log("Unable to insert module");
					debug("errmodule");
				});					

			}     		
		}).error(function(errbox)
		{
			console.log("Error finding box");
			debug(errbox);
		});
	}
}
