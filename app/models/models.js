var Sequelize = require('./sequelizeconnexion.js').Sequelize;
var sequelize = require('./sequelizeconnexion.js').sequelize;


// USER MODEL
var User = sequelize.define('bw_user',
{
	u_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement:true },
	u_name: { type: Sequelize.STRING },
	u_gid: { type: Sequelize.STRING },
	u_email: { type: Sequelize.STRING }
},
{
	classMethods:
	{
		associate: function(models)
		{
		//			User.belongsTo(models.UserBox, { as: 'ub_fk_user_id' });
	}
},
				// Needed to avoid CreatedAt in table, and avoir automatic plurals word
				timestamps : false,
				freezeTableName: true
//				tableName: 'user'
}
);
//var UB = require('./userbox.js').UserBox;
// USERBOX MODEL
var UserBox = sequelize.define('bw_userbox',
{
	ub_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement: true },
	ub_userboxname: { type: Sequelize.STRING },
	ub_userboxdescription:  { type: Sequelize.TEXT }
},
{
	classMethods:
	{
		associate: function(models)
		{

		}
	},

				   // Needed to avoid CreatedAt in table, and avoir automatic plurals word
				   timestamps : false,
				   freezeTableName: true
//				   tableName: 'userbox'

}
);
// BOX PURCHASE
var Purchase = sequelize.define('bw_purchase',
{
	p_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement: true },
	p_buyername: { type: Sequelize.STRING }			    
},
{
	classMethods:
	{
		associate: function(models)
		{

		}
	},
	timestamps : false,
	freezeTableName: true
}
);

// BOX MODEL
var Box = sequelize.define('bw_box',
{
	b_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement: true },
	b_lastip: { type: Sequelize.STRING },
	b_connected : { type: Sequelize.BOOLEAN },
	b_lastconnexion: { type: Sequelize.DATE },
	b_identifier: { type: Sequelize.STRING },
	b_password: { type: Sequelize.STRING }
			       // b_fk_ba_id: { type: Sequelize.INTEGER },
			       // b_fk_p_id: { type: Sequelize.INTEGER }
			   },
			   {
			   	classMethods:
			   	{
			   		associate: function(models)
			   		{

			   		}
			   	},

			       // Needed to avoid CreatedAt in table, and avoir automatic plurals word
			       timestamps : false,
			       freezeTableName: true
			   }
			   );

// USERBOX MODEL
var Module = sequelize.define('bw_module',
{
	m_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement: true },
	m_moduleid: { type: Sequelize.INTEGER },
	m_modulename: { type: Sequelize.STRING },
	m_description:  { type: Sequelize.TEXT }
},
{
				  // Needed to avoid CreatedAt in table, and avoir automatic plurals word
				  timestamps : false,
				  freezeTableName: true
				}
				);
var Switch =  sequelize.define('bw_switch',
{
	s_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement: true },
	s_instance: { type: Sequelize.INTEGER },
	s_level:  { type: Sequelize.INTEGER }
},
{
				   // Needed to avoid CreatedAt in table, and avoir automatic plurals word
				   timestamps : false,
				   freezeTableName: true
				}
				);
var SensorMultiLevel =  sequelize.define('bw_sensormultilevel',
{
	sml_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement: true },
	sml_instance: { type: Sequelize.INTEGER },
	sml_scale: { type: Sequelize.STRING },
	sml_level:  { type: Sequelize.INTEGER },
	sml_smlid:  { type: Sequelize.INTEGER, unique: true }
},
{
	classMethods:
	{
		associate: function(models)
		{

		}
	},							
					     // Needed to avoid CreatedAt in table, and avoir automatic plurals word
					     timestamps : false,
					     freezeTableName: true
					 }
					 );

var SensorBinary =  sequelize.define('bw_sensorbinary',
{
	sb_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement: true },
	sb_instance: { type: Sequelize.INTEGER },
	sb_level:  { type: Sequelize.INTEGER },
},
{
	classMethods:
	{
		associate: function(models)
		{

		}
	},							
					     // Needed to avoid CreatedAt in table, and avoir automatic plurals word
					     timestamps : false,
					     freezeTableName: true
					 }
					 );


// BOX APPLICATION
var BoxApplication = sequelize.define('bw_boxapplication',
{
	ba_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement: true },
	ba_identifier: { type: Sequelize.STRING },
	ba_password:  { type: Sequelize.STRING }
},
{
					  // Needed to avoid CreatedAt in table, and avoir automatic plurals word
					  timestamps : false,
					  freezeTableName: true
					}
					);

// ROOM/GROUP MODELS
var Room = sequelize.define('bw_room',
{
	r_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement: true },
	r_name: { type: Sequelize.STRING },
},
{
	classMethods:
	{
		associate: function(models)
		{

		}
	},

				// Needed to avoid CreatedAt in table, and avoir automatic plurals word
				timestamps : false,
				freezeTableName: true
			}
			);
var RoomModule = sequelize.define('bw_roommodule',
{
	rm_id: { primaryKey: true, type: Sequelize.INTEGER, autoincrement: true },
//				      rm_fk_module_id:  { type: Sequelize.INTEGER, unique: true }

},
{
	timestamps : false,
	freezeTableName: true
});


Room.belongsTo(User, { foreignKey: 'r_fk_user_id'});
User.hasOne(Room, { foreignKey: 'r_fk_user_id' });
Room.hasOne(RoomModule, { foreignKey: 'rm_fk_room_id'});
RoomModule.belongsTo(Room, { foreignKey: 'rm_fk_room_id' });

Room.belongsTo(Box, {foreignKey: 'r_fk_box_id', targetKey: 'b_id'}); ///////////////USE THIS !
//Box.belongsTo(Room, { as: 'r_fk_box_id', foreignKey: 'b_id' });


RoomModule.belongsTo(Module, {foreignKey: 'rm_fk_module_id', targetKey: 'm_moduleid', onDelete: 'CASCADE'});
//Module.hasOne(RoomModule, {foreignKey: 'rm_fk_module_id', targetKey: 'm_moduleid'});

// ADD FOREIGNKEY TO BOX


Box.belongsTo(BoxApplication, { foreignKey: 'b_fk_ba_id', targetKey: 'ba_id' });
BoxApplication.hasOne(Box, {foreignKey: 'b_fk_ba_id' });
//safe save reverse if switch fails or cause problem
//Box.hasOne(BoxApplication, { as: 'b_fk_ba_id', foreignKey: 'ba_id' });
//Box.hasOne(Purchase, { as: 'b_fk_p_id', foreignKey: 'p_id' });

User.hasOne(UserBox, {/* as: 'ub_fk_user_id', */foreignKey: 'ub_fk_user_id' });
UserBox.belongsTo(User, { foreignKey: 'ub_fk_user_id' });

Box.hasOne(UserBox, { foreignKey: 'ub_fk_box_id'});
UserBox.belongsTo(Box, { foreignKey: 'ub_fk_box_id' });


//Box.hasOne(Box, { foreignKey: 'm_fk_box_id'});
Module.belongsTo(Box, { foreignKey: 'm_fk_box_id'});

Switch.belongsTo(Module, { foreignKey: 's_fk_module_id', targetKey: 'm_moduleid', onDelete: 'CASCADE' });
//Module.hasOne(Switch, { foreignKey: 'm_module' });

SensorMultiLevel.belongsTo(Module, {foreignKey: 'sml_fk_module_id', targetKey:'m_moduleid', onDelete: 'CASCADE' });
SensorBinary.belongsTo(Module, {foreignKey: 'sb_fk_module_id', targetKey:'m_moduleid', onDelete: 'CASCADE' });
//Module.belongsTo(SensorMultiLevel, {foreignKey: 'm_moduleid'});
exports.SensorBinary = SensorBinary;
exports.RoomModule = RoomModule;
exports.Switch = Switch;
exports.SensorMultiLevel = SensorMultiLevel;
exports.BoxApplication = BoxApplication;
exports.Module = Module;
exports.Box = Box;
exports.User = User;
exports.UserBox = UserBox;
exports.Room = Room;
