// those variable before module.exports should be in a separate file and import like a module (didn't find out how yet)

//var sqlconnection = require('./database.js');

var debug = require('debug')('box');
//var Box = require('./models/box.js').Box;

// Used for color in debug
var colors = require('colors/safe');
var errorc = colors.red;
var infoc = colors.cyan;
var warnc = colors.yellow;
var reqc = colors.green;
var logc = colors.white;

// load model for sequelize
var Box = require('./models/models.js').Box;
var User = require('./models/models.js').User;
var UserBox = require('./models/models.js').UserBox;
module.exports =
    {

	boxid: -1,

	addbox: function addbox(myEmitter, origin, sender, next, box_id, clientid, boxname, boxdescription, password)
	{
	    Box.findAll({where: {b_identifier: box_id, b_password: password}}).then(
		function(boxrow)
		{
		    if (boxrow.length <= 0)
			console.log("No box with this id/password found");
		    else
		    {
			console.log("Row of box : ", boxrow[0].dataValues);
			UserBox.findAll({
			    include:
			    [
				{
				    model: Box,
				    where: {b_identifier: boxrow[0].dataValues.b_identifier, b_password: boxrow[0].dataValues.b_password},
				    required: true
				},
				{
				    model: User,
				    required: true,
				    where: {u_gid: clientid}
				}
			    ]
			}).then(
			    function(userboxrow)
			    {
				if (userboxrow.length > 0)
				{
				    debug("Box exist, updating");
				    if (!boxdescription)
					boxdescription = userboxrow[0].dataValues.ub_userboxdescription;
				    UserBox.update(
					{
					    ub_userboxname: boxname,
					    ub_userboxdescription: boxdescription,
					},
					{
					    where:
					    {
							ub_fk_box_id: userboxrow[0].dataValues.bw_box.dataValues.b_id
					    }
					}
				    ).then(function(rowupdated)
				    {
				    	debug("Row updated : " + rowupdated);
				    	if (!rowupdated)
				    		var message = "No box updated";
				    	else
				    		var message = "box successfully updated";
				    	myEmitter.emit("eventCrossroad", origin, sender, next, 200, message);
				    }).error(function(userboxrowerr)
					    {
							console.log("error updating users box");
							console.log(userboxrowerr);
					    	myEmitter.emit("eventCrossroad", origin, sender, next, 400, "Error updating users box");
					    });
				}
				else
				{
				    User.findAll({ attributes: ['u_id'], where: { u_gid: clientid } }).then(
					function(userrow)
					{
					    if (userrow.length <= 0)
					    {
						debug("No user found to add box");
				    	myEmitter.emit("eventCrossroad", origin, sender, next, 400, "No user founded to add this box");
						return;
					    }
					    debug("Adding box to user");
					    UserBox.create(
						{
						    ub_userboxname: boxname,
						    ub_userboxdescription: boxdescription,
						    ub_fk_box_id: boxrow[0].dataValues.b_id,
						    ub_fk_user_id: userrow[0].dataValues.u_id
						}).then(function(rowcreated)
						{
							if (rowcreated)
						    	myEmitter.emit("eventCrossroad", origin, sender, next, 200, "Box successfully added");
						    else
			   			    	myEmitter.emit("eventCrossroad", origin, sender, next, 400, "Unable to add box");
						}).error(function(errcreation)
						{
							debug("Unable to add box");
					    	myEmitter.emit("eventCrossroad", origin, sender, next, 400, "Error when adding box");
						});    
					}).error(function(usererr)
						 {
						     console.log("Error getting user id to add box");
						     console.log(usererr);
		     		    	 myEmitter.emit("eventCrossroad", origin, sender, next, 400, "Error getting user id to add box");
						 });
				}
			    });
		    }
		}).error(function(boxerr)
			 {
			     debug("Error when  fetching box");
			     debug(boxerr);
			 });
	},
	
	getboxid: function getboxid(myEmitter, origin, sender, next, message, clientID, wantedbox)
	{
	    if (next)
	    {
		if (next.length > 0)
		    next.shift();
		if (next.length == 0)
		    next = null;
	    }
	    Box.findAll({attributes: ['b_id'], where: {b_identifier: wantedbox}}).then(function(rowsbox)
								 {
								     debug("Selecting all box with b_identifier : %s", wantedbox);
								     if (rowsbox.length > 0)
								     {
									 debug("find rows with values : %s !", rowsbox[0].dataValues);
									 console.log(rowsbox[0].dataValues);
									 module.exports.boxid = rowsbox[0].dataValues;
									 //myEmitter.emit('e-callback', "box", "Box with identifier : " + wantedbox + " found and has for b_id : " + rowsbox[0].dataValues, null);
									 myEmitter.emit("eventCrossroad", origin, "getboxid", next, 200, rowsbox[0].dataValues);
								     }
								     else
								     {
									 debug("no b_id was found for box with b_id : %s", wantedbox);
									 next = origin;
									 myEmitter.emit("eventCrossroad",origin, "getboxid", next, 400, "BAD REQUEST : Error fetching box");
									 // myEmitter.emit('e-callbackfinal', 400, "error no box found");
								     }
								 }).error(function(err)
									 {
									     debug("Error when getting b_id from box %s!", wantedbox);
									     debug("Error is n ot send back : %s!", err);
									     next = origin;
									     myEmitter.emit("eventCrossroad", origin, "getboxid", next, 400, "BAD REQUEST : Error fetching box");
									     // myEmitter.emit('e-callbackfinal', 400, "BAD REQUEST : Error fetching box");
									 });
	},
	getuserbox: function(myEmitter, origin, sender, next, message, clientid)
	{
	    debug(infoc("Getting user's box for clientID : %s"), clientid);
	    if (next)
	    {
		if (next.length > 0)
		    next.shift();
		if (next.length == 0)
		    next = null;
	    }
	    UserBox.findAll
	    ({
		attributes: ['ub_userboxname', 'ub_userboxdescription'],
		include:
		[
		    {
			model: User,
			required: true,
			where: {u_gid: clientid}
		    },
		    {
			model: Box,
			attributes: ['b_id', 'b_identifier',  'b_lastip', 'b_connected', 'b_lastconnexion'],
			required:true,
		    }
		]
	    }).then(function (row)
		    {
			console.log("We are in then of getuserbox");
			var err = null;
			var table = [];
			if (row  && row.length > 0)
			{
			    debug("At least one row finded");
			    for (i in row)
			    {
				// console.log(row[i].dataValues.ub_userboxname, " - ", row[i].dataValues.ub_userboxdescription, " - ", row[i].dataValues.bw_box.dataValues);
				// We keep this formating, If we change here, be sure to modify other function who will need this result
				table.push({'b_id':row[i].dataValues.bw_box.dataValues.b_id, 'b_identifier':row[i].dataValues.bw_box.dataValues.b_identifier, 'ub_userboxname': row[i].dataValues.ub_userboxname,
					    'ub_userboxdescription': row[i].dataValues.ub_userboxdescription, 'b_lastip': row[i].dataValues.bw_box.dataValues.b_lastip,
					    'b_connected': row[i].dataValues.bw_box.dataValues.b_connected, 'b_lastconnexion': row[i].dataValues.bw_box.dataValues.b_lastconnexion});
			    }
			    debug(table);
			    code = 200;
			}
			else
			{
			    log = "\t=> " + warnc("No box found for this user clientid : ") + logc(clientid);
			    debug(log);
			    table = "No box found for this user";
			    next = null;
			    code = 400;
			}
			console.log("Leaving function getbox");
			debug("Code send in event : ", code, " - Message send in event : ", table);
			myEmitter.emit('eventCrossroad', origin, "getbox", next, code, table);
			return;
		    }).error(function (err)
			     {
				 log = "\t=> " + error("Error getting user's box. Error is not send back : ") + logc(err);
				 debug(log);
				 next = null;
				 myEmitter.emit('eventCrossroad', origin, "getbox", next, code, table);
				 return;
			     });
	}
	
    }

