var server = require('http').createServer()
, url = require('url')
, WebSocketServer = require('ws').Server
, wss = new WebSocketServer({ server: server })
, express = require('express')
, app = express()
, port = 8001;

app.use(function (req, res) {
    res.send({ msg: "Connected" });
});

wss.on('connection', function connection(ws) {
    var location = url.parse(ws.upgradeReq.url, true);

    ws.on('message', function incoming(message) {
	console.log('From: %s', ws.upgradeReq.connection.remoteAddress);
	console.log('Received: %s', message);
	if (ws.upgradeReq.connection.remoteAddress != "::ffff:192.168.1.199")
	    wss.broadcast(message);
    });

    ws.send('Echo');

});
wss.broadcast = function broadcast(data) {
    wss.clients.forEach(function each(client) {
	console.log('try to broadcast to : %s', client.upgradeReq.connection.remoteAddress);
	client.send(data);
    })};

server.on('request', app);
server.listen(port, function () { console.log('Listening on ' + server.address().port) });


