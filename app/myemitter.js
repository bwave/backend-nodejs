var events = require('events');
var debug = require('debug');
var myEmitter = new events.EventEmitter();
var command = require('./commands.js');
var module = require('./modules.js');
var box = require('./boxs.js');
var async = require('async');// Next is a array of listed event who need to be called. At every call, use shift() to delete the first element

    function callbackAsyncGetModule(sender, err)
    {
	console.log("MODULE CALLBACK SENDER : ", sender, "  ee:", err);
	
	if (err)
	{
	    console.log(err);
	    myEmitter.emit('e-callbackfinal', 400, "unable to fetch module");
	    return;
	}
	else
	    switch (sender)
	{
	    case 'module':
	    myEmitter.emit('e-callback', "module", "Module successfully fetched", null);
	    break;
	    case 'switch':
	    myEmitter.emit('e-callback', "switch", "Switch successfully fetched", null);
	    break;
	    case 'sensor':
	    myEmitter.emit('e-callback', "sensor", "Sensor successfully fetched", null);
	    break;
	}
    }

function eventCrossroad(origin, sender, next, code, message)
    {
	if (origin == "r_getallmodules")
	{
	    debug("In first if : ", code, " ,", origin, " :(-, ", sender);
	    if (code == 200)
		myEmitter.emit("e-callback", "box", message, null);
	    else
		myEmitter.emit("e-callback", "box", null, message);
	    return;
	}
	if (next == null || next.length == 0)
	{
	    console.log("No more function need to be call, callback origin : ", origin);
	    debug("sender : ", sender, " | code :", code, " | message : ", message);
	    if (origin == null)
		return console.log("Error, origin can't be null, caller : ", sender);
	    myEmitter.emit(origin, code, message); // we are going back to our route, we dont need sender and next anymore
	}
	else if (next.length > 0)
	{
	    debug(next);
	    console.log("Calling next function with event : ", next[0], " | from :", sender);
	    debug("Origin : ", origin, " | sender : ", sender, " | next : ", next, " | code :", code, " | message : ", message);
	    myEmitter.emit(origin, sender, next[0], code, message);
	}
    }
    // Maybe this function should go in another files ?
    var ecallback = function(sender, message, err)
    {
	switch (sender)
	{
	    case "box":
	    command.command_list  = [];
	    module.module_list = [];
	    // GET Module list
	    console.log("Calling getmodule => ", sender);
	    async.eachSeries(box.boxid, module.getmodule,callbackAsyncGetModule.bind(callbackAsyncGetModule, "module"));
	    break;
	    case "module":
	    moduleresult = true;
	    console.log("Module request executed successfully");
	    // Get Switch command
	    console.log("Calling getswitcommand", module.module_list[0]);
	    async.eachSeries(module.module_list[0],command.getswitchcommand, callbackAsyncGetModule.bind(callbackAsyncGetModule, "switch"));
	    // Get SensorrMultiLevel command
	    console.log("Calling getssensorcommand");
	    async.eachSeries(module.module_list[0],command.getsensormlvl, callbackAsyncGetModule.bind(callbackAsyncGetModule, "sensor"));
	    break;
	    case "switch":
	    switchresult = true;
	    if (moduleresult && switchresult && sensorresult)
	    {
		console.log("allgood switch");
		if (err)
		{
		    console.log("switch failure");
		    myEmitter.emit('r_getallmodules',400, "failed"); 
		}
		else
		{
		    console.log("switch failure");
		    myEmitter.emit('r_getallmodules', 200);
		}
	    }
	    break;
	    case "sensor":
	    sensorresult = true;
	    if (moduleresult && switchresult && sensorresult)
	    {
		console.log("If allgood sensor");
		if (err)
		{
		    console.log("sensor failure");
		    myEmitter.emit('r_getallmodules',400, "failed"); 
		}
		else
		{
		    console.log("sensor succes");
		    myEmitter.emit('r_getallmodules', 200, "success");
		}
	    }
	    break;
	}
    }
    myEmitter.addListener('e-callback', ecallback);
    myEmitter.addListener('eventCrossroad', eventCrossroad);
    
exports.myEmitter = myEmitter;
