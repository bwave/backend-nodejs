
//var sqlconnection = require('./database.js');
var debug = require('debug')('command');
// load model for sequelize
var Switch = require('./models/models.js').Switch;
var UserBox = require('./models/models').UserBox;
var Box = require('./models/models').Box;
var Module = require('./models/models').Module;
var User = require('./models/models').User;
var SensorMultiLevel = require('./models/models.js').SensorMultiLevel;
var SensorBinary = require('./models/models').SensorBinary;
///!\/!\   BECAREFULL MAYBE module.exports.command_list is not a proper way to use the command_list variable (didn't find out how to do it, keeping the name:function notation     /!\/!\
///!\/!\   BECAREFULL MAYBE module.exports.command_list is not a proper way to use the command_list variable (didn't find out how to do it, keeping the name:function notation     /!\/!\
///!\/!\   BECAREFULL MAYBE module.exports.command_list is not a proper way to use the command_list variable (didn't find out how to do it, keeping the name:function notation     /!\/!\
module.exports = 
{
	command_list: [], 

	getswitchcommand: function getswitchcommand(tt, callback2)
	{

		debug("Module for getswitch : %s", tt["m_moduleid"], "+++++++++++++++", module.exports.command_list);
		Switch.findAll({attribute:['s_fk_module_id'], where: {s_fk_module_id: tt["m_moduleid"]}}).then(function(rows)
		{
			if (rows.length > 0)
			{
				var flag = false;
				debug("Get switch command list successfully executed");
				for (k in module.exports.command_list)
				{
					console.log("Loop switch  :  ", module.exports.command_list[k].module.m_moduleid, " --------- ", tt["m_moduleid"], " |||| length : ", module.exports.command_list.length)
					if (module.exports.command_list[k].module.m_moduleid == tt["m_moduleid"])
					{
						debug("Rising flag MAGUEUL");
						flag = true;
					}
				}
				if (flag == false)
					module.exports.command_list.push({"module": tt, "command": []});
				for (j in module.exports.command_list)
				{
					for (i in rows)
					{
						if (module.exports.command_list[j].module && (module.exports.command_list[j].module.m_moduleid == tt["m_moduleid"]))
							module.exports.command_list[j]["command"].push({"CommandName":"Switch","commands":rows[i].dataValues});					    
						console.log("SWIIIIIIIIIIIIIIIITCH : ", rows[i].dataValues)
					}
	//else
	//											    module.exports.command_list.push({"room":roomrow[i].bw_room.dataValues, "module" : []});
}
}
//									    console.log("CALLING TT : ", tt);
callback2(null, tt);
}).error(function(err)
{
	debug(err);
	callback2(err, null);
	return;
});
},
getsensormlvl: function getsensormlvlcommand(q, callback)
{
	SensorMultiLevel.findAll({where: {sml_fk_module_id: q["m_moduleid"]}}).then(function (rows)
	{
		var flag = false;
		debug("Get SensorMmultiLevel command list successfully executed");
		for (k in module.exports.command_list)
		{
			console.log("Loop SML  :  ", module.exports.command_list[k].module.m_moduleid, " --------- ", q["m_moduleid"], " |||| length : ", module.exports.command_list.length)
			if (module.exports.command_list[k].module.m_moduleid == q["m_moduleid"])
			{
				debug("RED FLAG");
				flag = true;
			}
		}
		if (flag == false)
													module.exports.command_list.push({"module": q, "command": []});//
												//module.exports.command_list.push({"module": q, "command": []});
	//											console.log("BF PUSH : ", module.exports.command_list.length, " ====", module.exports.command_list.module);
	console.log	("Swml q : ", q)
	for (j in module.exports.command_list)
	{
		for (i in rows)
		{
//												    	console.log("Loop sml, show command : ", module.exports.command_list[j].module.m_moduleid, "----------------------------", rows[i].dataValues)
if (module.exports.command_list[j].module && (module.exports.command_list[j].module.m_moduleid == q["m_moduleid"]))
	module.exports.command_list[j]["command"].push({"CommandName":"SensorMultiLevel", "commands":rows[i].dataValues});
													}									//										    else
		//											    module.exports.command_list.push({"room":roomrow[i].bw_room.dataValues, "module" : []});
	}
	//										        module.exports.command_list[index]["command"].push({"CommandName":"SensorMultiLevel", "commands":rows[i].dataValues});
	callback(null, q);
}).error (function(err)
{
	debug(err);
	callback(err, null);
});
},
saveSwitch: function saveSwitch(ba_id, moduleid, name, id, instance, level, scale)
{
	Box.findAll({ where: {b_fk_ba_id: ba_id}}).then(function(rowbox)
	{
		if (rowbox.length > 0)
		{
//                              moduleid = 7;                                                                                                                                                                                                 
		console.log("Save Switch : ", rowbox[0].dataValues.b_id, " NAME : ", name, "  moduleid : ", moduleid, " id : ", id, " instance : ", instance, " level : ", level);
		Switch.upsert(
		{
			s_level: level,
			s_instance: instance,
			s_fk_module_id: moduleid
		}).then(function (rowswitch)
		{
			debug(rowswitch);
			if (rowswitch)
				console.log("Switch successfully saved : " + id + " | " + instance + " | level");
			else 
				console.log("Switch successfully updated");
		}).error(function (errswitch)
		{
			console.log("Error saving switch");
			debug(errswitch);
		});
		}
	}).error(function (errbox)
		{
			console.log("Error fetching box application");
			debug(errbox);
		});
},
saveSensorMultiLevel: function saveSensorMultiLevel(ba_id, moduleid, name, id, instance, level, scale)
{
	Box.findAll({ where: {b_fk_ba_id: ba_id}}).then(
		function(rowbox)
		{
			if (rowbox.length > 0)
			{
				console.log("Save sensor : ", rowbox[0].dataValues.b_id, "  moduleid : ", moduleid, " id : ", id, " instance : ", instance, " level : ", level, " scale :", scale);

                    SensorMultiLevel.upsert(
                    {
                    	sml_level: level,
                    	sml_instance: instance,
                    	sml_scale: scale,
                    	sml_smlid: id,
                    	sml_fk_module_id: moduleid,
                    }).then(function (rowsensor)
                    {
                    	console.log("row created : ", rowsensor);
                    	if (rowsensor)
                    		console.log("SensorMultiLevel successfully saved : " + id + " | " + instance + " | level");
                    	else
                    		console.log("SensorMultivlevel updated");
                    }).error(function (errsensor)
                    {
                    	console.log("Error saving Sensor");
                    	debug(errsensor);
                    });
            }
            else
            	console.log("No box finded");
        }).error(function (errbox)
            {
            	console.log("Error fetching box application");
            	debug(errbox);
            })
        },		
saveSensorBinary: function saveSensorBinary(ba_id, moduleid, name, id, instance, level)
{
	Box.findAll({ where: {b_fk_ba_id: ba_id}}).then(
		function(rowbox)
		{
			if (rowbox.length > 0)
			{
				console.log("Save sensorBinary : ", rowbox[0].dataValues.b_id, "  moduleid : ", moduleid, " id : ", id, " instance : ", instance, " level : ", level);

                    SensorBinary.upsert(
                    {
                    	sb_level: level,
                    	sb_instance: instance,
                    	sb_fk_module_id: moduleid,
                    }).then(function (rowsensor)
                    {
                    	console.log("row created : ", rowsensor);
                    	if (rowsensor)
                    		console.log("SensorBinary successfully saved : " + id + " | " + instance + " | level");
                    	else
                    		console.log("SensorBinary updated");
                    }).error(function (errsensor)
                    {
                    	console.log("Error saving Sensor");
                    	debug(errsensor);
                    });
            }
            else
            	console.log("No box finded");
        }).error(function (errbox)
            {
            	console.log("Error fetching box application");
            	debug(errbox);
            })
        },		
    // /!\ Updatecommand should check if the user own this command before updating it, otherwise (this is happening now), everybody can modify command of anyone
    // /!\ Updatecommand should check if the user own this command before updating it, otherwise (this is happening now), everybody can modify command of anyone
    // /!\ Updatecommand should check if the user own this command before updating it, otherwise (this is happening now), everybody can modify command of anyone
    updateSwitch: function updateSwitch(myEmitter, origin, sender, next, clientid, name, commandid, level, boxid)
    {
	//    	boxid = [99,98,87];
	Switch.update(
	{
		s_level: level
	},
	{
		where:
		{
			s_id: commandid
		},
	}).then(function(rowupdated)
	{
		if (rowupdated > 0)
		{
			var code = 200;
			var message = "Command " + name + " successfully updated";
		}
		else
		{
			var code = 400;
			var message = "Unable to update command " + name;
		}
		debug("Switch updated ? :" + code + " | " + message);
		myEmitter.emit("eventCrossroad", origin, sender, next, code, message);
		return;
	}).error(function(err)
	{
		console("Error updating Switch command");
		debug(err);
		myEmitter.emit("eventCrossroad", origin, sender, next, 500, "Error while updating switch command");
		return;
	});
},
updateSensormultilevel: function updateSensormultilevel(myEmitter, origin, sender, next, clientid, name, commandid, level, scale)
{
//    	boxid = [99,98,87];
	SensorMultiLevel.update(
	{
		sml_level: level,
		sml_scale: scale
	},
	{
		where:
		{
			sml_id: commandid
		}
	}).then(function(rowupdated)
	{
		if (rowupdated > 0)
		{
			var code = 200;
			var message = "Command " + name + " successfully updated";
		}
		else
		{
			var code = 400;
			var message = "Unable to update command " + name;
		}
		myEmitter.emit("eventCrossroad", origin, sender, next, code, message);
		return;
	}).error(function(err)
	{
		console.log("Error while updating sensormultilevel");
		myEmitter.emit("eventCrossroad", origin, sender, next, 500, "Error while updating sensormultilevel");
		return;
});
},
updatecommand: function updatecommand(myEmitter, origin, sender, next, clientid, name, commandid, level, scale)
{	
	UserBox.findAll
	({
		attributes: ['ub_fk_box_id'],
		include:
		[
		{
			model: User,
			required: true,
			attributes: [],
			where: {u_gid: clientid}
		},
		{
			model: Box,
			attributes: [],
			required:true,
		}
		]}).then(function(rowuserbox)
		{
			if (rowuserbox.length > 0)
			{
				console.log("Ub : ", rowuserbox);
				var table = [];
				for (i in rowuserbox)
				{
					table.push(rowuserbox[i].dataValues['ub_fk_box_id']);
				}
				Module.findAll({
					where:{ m_fk_box_id : table}
				}).then(function(rowmoduleapproved)
				{
					if (rowmoduleapproved.length > 0)
					{
						if (name == "Switch")
						{
							module.exports.updateSwitch(myEmitter, origin, sender, next, clientid, name, commandid, level);
						}
						else if (name == "Sensormultilevel")
							module.exports.updateSensormultilevel(myEmitter, origin, sender, next, clientid, name, commandid, level, scale);
					}
					else
						myEmitter.emit("eventCrossroad", origin, sender, next, 400, "Unable to update command, no module found");
				}).error(function (errmoduleapproved)
				{
					console.log("Error checking module box id");
					debug(errmoduleapproved);
					myEmitter.emit("eventCrossroad", origin, sender, next, 500, "Error while getting module id");
				});
			}
		}).error(function (erruserbox)
		{
			console.log("Error checking user id");
			debug(erruserbox);
			myEmitter.emit("eventCrossroad", origin, sender, next, 500, "Error while getting user id");
		});
/*		if (name == "Sensormultilevel")
		{
					SensorMultiLevel.update(
			    	{
						sml_level: level,
						sml_scale: scale
				    },
				    {
					where:
					{
					    sml_id: commandid
					}
				    }).then(function(rowupdated)
				    {
						if (rowupdated)
						{
							var code = 200;
							var message = "Command " + name + " successfully updated";
						}
						else
						{
							var code = 400;
							var message = "Unable to update command " + name;
						}
						myEmitter.emit("eventCrossroad", origin, sender, next, code, message);
				    }).error(function(err)
					     {
							console.log("Error while updating sensormultilevel");
							myEmitter.emit("eventCrossroad", origin, sender, next, 500, "Error while updating sensormultilevel");
							return;
					     });
					 }*/
					}
				}
    /*
    getmodule: function getmodule(moduletable, query0, callback0)
{
//    console.log("GET MODULE PARAM: ", moduletable, " --- ", query0, " --- ", callback0);
    q = sqlconnection.query("SELECT * FROM bw_module WHERE bw_module.m_fk_box_id = ?", [query0], function(err, rs_module)
			    {
				console.log(q.sql);
				console.log("module in get : ", moduletable);
				console.log( "rs_module : ", rs_module);
				if (err)
				    console.log(err);
				else if (rs_module.length > 0)
				{
				    moduletable.push(rs_module);
				}
				callback0(null, rs_module);
			    });
},
*/
/*
    getcommand: function getmodulecallback(err)
{
    if (err)
	console.log(err);
    else
    {
	try
	{
	    // Get Switch command
	    console.log("Calling getswitcommand");
	    async.eachSeries(rs_module,switchmodule.getswitchcommand);
	    // Get SensorMultiLevel command
	    console.log("Calling getssensorcommand");
	    async.eachSeries(rs_module,sensormodule.getsensormlvl);
	}
	catch (err)
	{
	    console.log("***********************");
	    console.log(err);
	    //   console.log("-----  ", results, "  -----  ", " ---");
	}
    }
}
*/
