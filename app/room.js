var Room = require('./models/models.js').Room;
var RoomModule = require('./models/models.js').RoomModule;
var User = require('./models/models.js').User;
var Module = require('./models/models.js').Module;
var UserBox = require('./models/models.js').UserBox;
var debug = require('debug')('room');

module.exports =
{
	updateRoomModule: function(myEmitter, origin, sender, next, clientid, roomid, moduleid, boxid)
	{
		Room.findAll(
		{
//		    attributes: ['r_name'],
where: { r_id: roomid },
include:
[
{
	model: User,
	where: { u_gid: clientid },
	require: true
},
{
	model: Box,
	where: { b_id : boxid },
	require: true
}]
}).then(function (roomrow)
{
	debug("In updateRoomModule : ", roomrow);
	if (roomrow.length > 0)
	{
		RoomModule.upsert({rm_fk_module_id: moduleid, rm_fk_room_id: roomid}).then(function(roomupdated)
		{
			console.log("rooom ", roomid, "updated");
			debug(roomupdated);
			myEmitter.emit("eventCrossroad", origin, "updateroom", next, 200, "Room updated successfully");
		}).error(function(roomupdatederror)
		{
			console.log("Error updating room");
			debug(roomupdatederror);
			myEmitter.emit("eventCrossroad", origin, "updateroom", next, 500, roomupdatederror);
		});
	}
	else
	{
		debug("No room founded : ", roomrow);
		myEmitter.emit("eventCrossroad", origin, "updateroom", next, 400, "No room founded");
	}
}).error(function(roomerror)
{
	console.log("error upsert command in room");
	debug(roomerror);
	myEmitter.emit("eventCrossroad", origin, "updateroom", next, 500, roomerror);
});
},
getRoom: function(myEmitter, origin, sender, next, clientid, boxid)
{
	Room.findAll(
	{
		attributes: ['r_id', 'r_name'],
//		    where: { r_id: roomid },
include:
[
{
	model: User,
	attributes: [],
	where: { u_gid: clientid },
	require: true
},
{
	model: Box,
	attributes: [],
	where: { b_id: boxid},
	require: true
}]
	}).then(function (roomrow)
		{
			var table = [];
		    debug("In getRoom : ", roomrow);
		    if (roomrow.length <= 0)
		    {
			debug("User has no room");
			
			// EVENT EMITTER
			myEmitter.emit('eventCrossroad', origin, 'getroom', next, 200, table);
			return;
		    }
		    else
		    {
			debug("found room  :" + roomrow);
			console.log(roomrow);
			for (i in roomrow)
			{
			    debug("Users room : ", roomrow[i].dataValues);
			    console.log("Ysssss :: ", roomrow[i].dataValues);
			    table.push(roomrow[i].dataValues);
			}
			myEmitter.emit('eventCrossroad', origin, 'getroom', next, 200, table);
			return;	
			// EMITTER
		    }
		}).error(function (roomerror)
			 {
			     debug("Error when getting user's box");
			     debug(roomerror);
			     myEmitter.emit('eventCrossroad', origin, 'getroom', next, 500, 'Error when getting user room');				      //EMITER
			     return;
			 });
},
	deleteRoomModule: function(myEmitter, origin, sender, next, clientid, roomid, moduleid)
	{
		RoomModule.destroy(
		{
			where: { rm_fk_room_id: roomid, rm_fk_module_id: moduleid },
			include:
			[
			{
				model: Room,
				where: { r_id: roomid },
				require: true,
				include:
				[
				{
					model: User,
					where: {u_gid: clientid},
					require:true,
				},
				{
					model: Box,
					where: {b_id: boxid},
					require: true,
				}
				]
			}
			]
		}).then(function(rowdeleted)
		{
			debug("row deleted: ", rowdeleted);
			if (rowdeleted > 0)
			{
				myEmitter.emit('eventCrossroad', origin, 'deleteroom', next, 200, 'Module sucessfully deleted');
				return;//EMITER
			}
			else
			{
				myEmitter.emit('eventCrossroad', origin, 'deleteroom', next, 204, 'No module deleted');
				return;//EMITER
			}
		}).error(function (errordeleted)
		{
			debug("Failed to delete module in room");
			debug(errordeleted);
			myEmitter.emit('eventCrossroad', origin, 'deleteroom', next, 500, 'Unable to delete module in room');
						 return;//EMITER
						});
	},
    getRoomModule: function(myEmitter, origin, sender, next, clientid, roomid, boxid)
	{

		RoomModule.findAll(
		{
//		    where: { rm_fk_room_id: roomid },
attributes: [],
where: { rm_fk_room_id: roomid },
include:
[
{
	model: Module,
	require:true,
	attributes: ['m_moduleid', 'm_modulename', 'm_description']
},
{
	model: Room,
	where: { r_id: roomid },
	require: true,
	attributes: ['r_name', 'r_id'],
	include:
	[
	{
		model: User,
		where: {u_gid: clientid},
		require:true,
		attributes: []
	},
	{
		model: Box,
		where: { b_id: boxid },
		require: true
	}]
}
]
}).then(function (roomrow)
{
	debug("in getRoomModule: roomrow");
	if (roomrow.length <= 0)
	{
						var table = [];
					 // EVENT EMITTER
					 debug("No module finded for this room");
					 myEmitter.emit('eventCrossroad', origin, 'getRoomModule', next, 200, table);
					 return;
					}
					else
					{
						debug("Found module for this room  :" + roomrow);
						for (i in roomrow)
						{
							var index = 0;
							debug("Room modules : ", roomrow[i].dataValues);
							if (table.length <= 0)
								table.push({"room":roomrow[i].bw_room.dataValues, "module" : []});
							else
								for (j =0; j != (table.length - 1); j++)
								{

									if (table[j].room && (table[j].room.r_id = roomrow[i].bw_room.dataValues.r_id))
										index = j;
									else
										table.push({"room":roomrow[i].bw_room.dataValues, "module" : []});
								}
								table[index]["module"].push(roomrow[i].bw_module.dataValues);
							}
							myEmitter.emit('eventCrossroad', origin, 'getRoomModule', next, 200, table);
							return;	
				// EMITTER
			}
		}).error(function (roomerror)
		{
			debug("Error when getting room modules");
			debug(roomerror);
			myEmitter.emit('eventCrossroad', origin, 'getRoomModule', next, 500, "Error when getting room modules");
				      //EMITER
				      return;
				  });
	},
    createRoom: function(myEmitter, origin, sender, next, clientid, roomname,id, boxid)
	{
	    // THIS MIGHT BE REPLACED WITH FIND OR CREATE
	    User.findAll({ where: { u_gid: clientid } }).then(
	    	function (userrow)
	    	{
	    		if (userrow.length > 0)
	    		{
	    			Room.upsert(
	    			{
	    			    r_id: id, 
	    			    r_name: roomname,
	    			    r_fk_user_id: userrow[0].dataValues.u_id,
				    r_fk_box_id: boxid
	    			}).then(function(rowroom)
	    			{
	    				debug("Will this appear ?");
	    				myEmitter.emit('eventCrossroad', origin, 'createroom', next, 200, 'Room successfully updated');
	    				return;
	    			}).error(function(err)
	    			{
	    				debug("Error creating new room");
	    				debug(err);
					     myEmitter.emit('eventCrossroad', origin, 'createroom', next, 500, 'Error when creating room');				      //EMITER
					     return;
					 });
	    		}
	    		else
	    		{
	    			debug("Error finding user");
	    			myEmitter.emit('eventCrossroad', origin, 'createroom', next, 400, 'No user was found with this id');
	    			return;
	    		}
	    	}).error(function(erruser)
	    	{
	    		debug("Error finding user id for created room");
	    		debug(erruser);
	    		myEmitter.emit('eventCrossroad', origin, 'createroom', next, 500, 'Error when getting user to create room');
			     return;//EMITER
			 });
	    },

	    deleteRoom: function(myEmitter, origin, sender, next, clientid, roomid)
	    {
	    	User.findAll({ attributes: ['u_id'], where: { u_gid: clientid }}).then(
	    		function(rowuser)
	    		{
	    			if (rowuser.length > 0)
	    				Room.destroy({
	    					where: { r_id: roomid, r_fk_user_id: rowuser[0].dataValues.u_id }
	    				}).then(function (rowdeleted)
	    				{
	    					if (rowdeleted > 0)
	    					{
	    						myEmitter.emit('eventCrossroad', origin, 'deleteroom', next, 200, 'Room sucessfully deleted');
					return;//EMITER
				}
				else
				{
					myEmitter.emit('eventCrossroad', origin, 'deleteroom', next, 204, 'No room deleted');
					return;//EMITER
				}
//				    debug(rowdeleted);
}).error(function (errordeleted)
{
	debug("Failed to delete room");
	debug(errordeleted);

	myEmitter.emit('eventCrossroad', origin, 'deleteroom', next, 500, 'Unable to delete room');
					 return;//EMITER
					});
else
{
	debug("No user find for this room");
			myEmitter.emit('eventCrossroad', origin, 'createroom', next, 200, 'No user found to delete this room');			//EMITER
			return;
		}
	}).error(function(erroruser)
	{

		debug(erroruser);
		myEmitter.emit('eventCrossroad', origin, 'createroom', next, 500, 'Error when getting user to delete room');
		return;
	});
}

}
