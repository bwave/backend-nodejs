var User = require('./models/models.js').User;
var UserBox = require('./models/models.js').UserBox;
var debug = require('debug')('box');
module.exports =
    {
	getuser: function(myEmitter, clientid, email, name, from)
	{
	    User.findAll({where: {u_gid: clientid}}).then(
		function(userrow)
		{
		    if (userrow.length > 0)
		    {
			console.log(userrow);
			myEmitter.emit("check", true);
		    }else
			module.exports.createuser(myEmitter,clientid,email,name);
		}).error(function(err)
			 {
			     debug("Error getting user - getuser");
			     debug(err);
			     myEmitter.emit("check", false);
			 });
	},
	
	createuser: function(myEmitter, clientid, email, name)
	{
	    debug("clientid : ", clientid, " - ", email, " - ", name);
	    if (typeof(clientid) == "undefined" ||
		typeof(email) == "undefined" ||
		typeof(name) == "undefined")
		return console.log("Mandatory data not set");
	    debug("Creating new user with data : ", clientid, " ", name, " ", email);
	    User.create(
		{
		    u_name: name,
		    u_gid: clientid,
		    u_email: email
		}).then(function(results)
			{
			    console.log("Results of creating users : ", results);
			    if (results)
				myEmitter.emit("check", false);
			}).error(function(err)
			 {
			     debug("Error creating new user");
			     debug(err);
			     myEmitter.emit("check", false);			     
			 });
	    return;
	}
    }
