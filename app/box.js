// those variable before module.exports should be in a separate file and import like a module (didn't find out how yet)
var sqlconnection = require('./database.js');
var debug = require('debug')('box');
var colors = require('colors/safe');

// set single property
var errorc = colors.red;
var infoc = colors.cyan;
var warnc = colors.yellow;
var reqc = colors.green;
var logc = colors.white;


// load model for sequelize 
var 
module.exports =
    {
	boxid: -1,
	
	getboxid: function getboxid(myEmitter, clientID, wantedbox)
	{
	    boxselect = sqlconnection.query("SELECT b_id FROM bw_box WHERE b_identifier = ?", [wantedbox], function (err, rowsbox)
					    {
						debug("Selected sql : %s", boxselect.sql);
						debug("finded rows : %s !", rowsbox);
						if (err)
						{
						    debug("Error when getting b_id from box %s!", wantedbox);
						    debug("Error is not send back : %s!", err);
						    myEmitter.emit('e-callbackfinal', 400, "error fetching box");
						    return;
						}
						else if (rowsbox.length > 0)
						{
						    // We send rows[0] because we are supposed to get only 1 rows from the boxselect request;
						    debug("found box with id : %s and b_identifier = %s !", rowsbox[0], wantedbox);
						    module.exports.boxid = rowsbox[0];
						    myEmitter.emit('e-callback', "box", "Box with identifier : " + wantedbox + " found and has for b_id : " + rowsbox[0], err);
						    return;
						}
						else if (rowsbox.length <= 0)
						{
						    debug("no b_id was found for box with b_id : %s", wantedbox);
						    myEmitter.emit('e-callbackfinal', 400, "error no box found");
						    return;
						}
					    }
					   );
	},
	getuserbox: function(myEmitter, email, clientid)
	{
//	    clientid = "s";
	    query = sqlconnection.query("SELECT bw_box.b_identifier, bw_userbox.ub_userboxname, ub_userboxdescription, b_lastip, b_connected, b_lastconnexion FROM `bw_userbox` INNER JOIN bw_user ON bw_userbox.ub_fk_user_id = bw_user.u_id INNER JOIN bw_box ON bw_box.b_id = bw_userbox.ub_fk_box_id WHERE  bw_user.u_gid = ?",[clientid], function(err, rows)
					{
					    debug(infoc("Getting user's box for clientID : %s"), clientid);
					    debug(infoc("\t=> Request : ") + "%s", reqc(query.sql));
					    if (err)
						log = "\t=> " + error("Error getting user's box. Error is not send back : ") + logc(err);
					    else if (rows.length > 0)
						log = "\t=> " + infoc("At least one rows finded : ") + logc(rows);
					    else
					    {
						log = "\t=> " + warnc("No box found for this user clientid : ") + logc(clientid);
						err = "No box found for this user";
					    }
					    debug(log);
					    myEmitter.emit('e-getuserbox', err, rows);
					    return;
					    /*				    if (err)
				    {
					debug("\t=> "+ colors.red("Error getting user's box. Error is not send back :  %s"), colors.prompt(err));
					res.send(400, "Impossible to get box");
				    }
				    else if (rows.length > 0)
				    {
					debug("\t=> At least one rows finded : %s", rows);
					res.send(200, rows);
					
				    }
				    else
				    {
					debug("\t=> " + colors.warn("No box found for this user clientid : %s"), clientid);
					res.send(200, "No box found");
				    }
*/				}
			       );
	}	
    }

