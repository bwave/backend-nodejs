-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2016 at 04:06 PM
-- Server version: 5.7.13-0ubuntu0.16.04.2
-- PHP Version: 7.0.8-0ubuntu0.16.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bwave`
--

-- --------------------------------------------------------

--
-- Table structure for table `bw_box`
--

CREATE TABLE `bw_box` (
  `b_id` int(11) NOT NULL,
  `b_lastip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `b_connected` tinyint(1) NOT NULL,
  `b_lastconnexion` datetime NOT NULL,
  `b_identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `b_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `b_fk_ba_id` int(11) NOT NULL,
  `b_fk_p_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bw_box`
--

INSERT INTO `bw_box` (`b_id`, `b_lastip`, `b_connected`, `b_lastconnexion`, `b_identifier`, `b_password`, `b_fk_ba_id`, `b_fk_p_id`) VALUES
(1, '192.168.0.99', 1, '2016-07-17 12:12:30', 'userHugo', 'motdepasse', 2, 1),
(2, '', 0, '2016-09-06 04:08:07', '502', 'deroche', 1, 1),
(3, '', 0, '2016-09-05 07:14:15', '503', 'pasword', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bw_boxapplication`
--

CREATE TABLE `bw_boxapplication` (
  `ba_id` int(11) NOT NULL,
  `ba_identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ba_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bw_boxapplication`
--

INSERT INTO `bw_boxapplication` (`ba_id`, `ba_identifier`, `ba_password`) VALUES
(1, 'bentouille', 'motdepasse'),
(2, 'hEaeRtgQrCAtsRlfDavn4SHR4DCSXHoW', 'CmiSNldXX4WdvD6Dp779ieUHrM7gRilN'),
(3, 'otherbox1', 'otherpass1');

-- --------------------------------------------------------

--
-- Table structure for table `bw_clientapplication`
--

CREATE TABLE `bw_clientapplication` (
  `ca_id` int(11) NOT NULL,
  `ca_aud` int(11) NOT NULL,
  `ca_applicationname` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bw_module`
--

CREATE TABLE `bw_module` (
  `m_id` int(11) NOT NULL,
  `m_moduleid` int(11) NOT NULL,
  `m_modulename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `m_fk_box_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bw_module`
--

INSERT INTO `bw_module` (`m_id`, `m_moduleid`, `m_modulename`, `m_description`, `m_fk_box_id`) VALUES
(1, 8, NULL, NULL, 1),
(2, 10, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bw_purchase`
--

CREATE TABLE `bw_purchase` (
  `p_id` int(11) NOT NULL,
  `p_buyername` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bw_purchase`
--

INSERT INTO `bw_purchase` (`p_id`, `p_buyername`) VALUES
(1, 'hugo');

-- --------------------------------------------------------

--
-- Table structure for table `bw_room`
--

CREATE TABLE `bw_room` (
  `r_id` int(11) NOT NULL,
  `r_name` varchar(255) NOT NULL,
  `r_fk_user_id` int(11) NOT NULL,
  `r_fk_box_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_room`
--

INSERT INTO `bw_room` (`r_id`, `r_name`, `r_fk_user_id`, `r_fk_box_id`) VALUES
(3, 'library', 4, 1),
(7, 'TOOO', 4, 0),
(8, 'kitchen', 4, 0),
(9, 'ma cuisine', 4, 0),
(10, 'ma chambre', 4, 0),
(11, 'ma cuisines', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bw_roommodule`
--

CREATE TABLE `bw_roommodule` (
  `rm_id` int(11) NOT NULL,
  `rm_fk_room_id` int(11) NOT NULL,
  `rm_fk_module_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bw_sensorbinary`
--

CREATE TABLE `bw_sensorbinary` (
  `sb_id` int(11) NOT NULL,
  `sb_fk_module_id` int(11) NOT NULL,
  `sb_instance` int(11) NOT NULL,
  `sb_level` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_sensorbinary`
--

INSERT INTO `bw_sensorbinary` (`sb_id`, `sb_fk_module_id`, `sb_instance`, `sb_level`) VALUES
(7, 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bw_sensormultilevel`
--

CREATE TABLE `bw_sensormultilevel` (
  `sml_id` int(11) NOT NULL,
  `sml_instance` int(11) NOT NULL,
  `sml_scale` varchar(11) NOT NULL,
  `sml_level` int(11) NOT NULL,
  `sml_fk_module_id` int(11) NOT NULL,
  `sml_smlid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_sensormultilevel`
--

INSERT INTO `bw_sensormultilevel` (`sml_id`, `sml_instance`, `sml_scale`, `sml_level`, `sml_fk_module_id`, `sml_smlid`) VALUES
(1, 0, '°C', 27, 10, 1),
(3, 0, 'Lux', 127, 10, 3),
(4, 0, 'W', 0, 8, 4);

-- --------------------------------------------------------

--
-- Table structure for table `bw_switch`
--

CREATE TABLE `bw_switch` (
  `s_id` int(11) NOT NULL,
  `s_instance` int(11) NOT NULL,
  `s_level` tinyint(1) NOT NULL,
  `s_fk_module_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bw_switch`
--

INSERT INTO `bw_switch` (`s_id`, `s_instance`, `s_level`, `s_fk_module_id`) VALUES
(2, 0, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `bw_user`
--

CREATE TABLE `bw_user` (
  `u_id` int(11) NOT NULL,
  `u_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_gtoken` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_gid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user login data';

--
-- Dumping data for table `bw_user`
--

INSERT INTO `bw_user` (`u_id`, `u_name`, `u_gtoken`, `u_gid`, `u_email`) VALUES
(1, 'Toma C.F.', NULL, 'FnXpSGTabzruAjIWOlg0pwc6EFk7xdU7', 'toma.cristini@gmail.com'),
(2, 'aert', 'aerta', 'ertaer', 't'),
(3, 'bwavetest123', NULL, '118187645865548110712', 'bwavetest123@gmail.com'),
(4, 'Blastsider', NULL, '115526176632606751752', 'blastcodeurs@hotmail.fr'),
(5, 'seqName', NULL, 'seqtoken', 'seqemail@seq.seq'),
(31, 'Toma C.F.', NULL, '109803579477036886582', 'toma.cristini@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `bw_userbox`
--

CREATE TABLE `bw_userbox` (
  `ub_id` int(11) NOT NULL,
  `ub_fk_user_id` int(11) NOT NULL COMMENT 'id from user table',
  `ub_fk_box_id` int(11) NOT NULL COMMENT 'id from box table',
  `ub_userboxname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ub_userboxdescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bw_userbox`
--

INSERT INTO `bw_userbox` (`ub_id`, `ub_fk_user_id`, `ub_fk_box_id`, `ub_userboxname`, `ub_userboxdescription`) VALUES
(14, 1, 3, 'Maison de vacance', 'Maison de location à montoulieu'),
(16, 3, 2, 'boxup', 'final'),
(17, 3, 3, 'Totoboxxx', 'boxx'),
(18, 4, 1, 'new box name', 'my mdescription'),
(20, 4, 2, 'boxup', 'final');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bw_box`
--
ALTER TABLE `bw_box`
  ADD PRIMARY KEY (`b_id`),
  ADD KEY `b_fk_ba_id` (`b_fk_ba_id`),
  ADD KEY `b_fk_p_id` (`b_fk_p_id`);

--
-- Indexes for table `bw_boxapplication`
--
ALTER TABLE `bw_boxapplication`
  ADD PRIMARY KEY (`ba_id`);

--
-- Indexes for table `bw_clientapplication`
--
ALTER TABLE `bw_clientapplication`
  ADD PRIMARY KEY (`ca_id`);

--
-- Indexes for table `bw_module`
--
ALTER TABLE `bw_module`
  ADD PRIMARY KEY (`m_id`),
  ADD UNIQUE KEY `m_moduleid` (`m_moduleid`),
  ADD KEY `m_fk_box_id` (`m_fk_box_id`);

--
-- Indexes for table `bw_purchase`
--
ALTER TABLE `bw_purchase`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `bw_room`
--
ALTER TABLE `bw_room`
  ADD PRIMARY KEY (`r_id`),
  ADD KEY `r_fk_user_id` (`r_fk_user_id`),
  ADD KEY `r_fk_box_id` (`r_fk_box_id`);

--
-- Indexes for table `bw_roommodule`
--
ALTER TABLE `bw_roommodule`
  ADD PRIMARY KEY (`rm_id`),
  ADD KEY `rm_fk_room_id` (`rm_fk_room_id`),
  ADD KEY `rm_fk_module_id` (`rm_fk_module_id`);

--
-- Indexes for table `bw_sensorbinary`
--
ALTER TABLE `bw_sensorbinary`
  ADD PRIMARY KEY (`sb_id`),
  ADD UNIQUE KEY `sb_fk_module_id_2` (`sb_fk_module_id`),
  ADD KEY `sb_fk_module_id` (`sb_fk_module_id`);

--
-- Indexes for table `bw_sensormultilevel`
--
ALTER TABLE `bw_sensormultilevel`
  ADD PRIMARY KEY (`sml_id`),
  ADD UNIQUE KEY `sml_smlid` (`sml_smlid`),
  ADD KEY `sml_fk_module_id` (`sml_fk_module_id`),
  ADD KEY `sml_fk_module_id_2` (`sml_fk_module_id`);

--
-- Indexes for table `bw_switch`
--
ALTER TABLE `bw_switch`
  ADD PRIMARY KEY (`s_id`),
  ADD UNIQUE KEY `s_fk_module_id_2` (`s_fk_module_id`),
  ADD KEY `s_fk_module_id` (`s_fk_module_id`);

--
-- Indexes for table `bw_user`
--
ALTER TABLE `bw_user`
  ADD PRIMARY KEY (`u_id`),
  ADD UNIQUE KEY `u_gid` (`u_gid`);

--
-- Indexes for table `bw_userbox`
--
ALTER TABLE `bw_userbox`
  ADD PRIMARY KEY (`ub_id`),
  ADD KEY `ub_fk_user_id` (`ub_fk_user_id`),
  ADD KEY `ub_fk_box_id` (`ub_fk_box_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bw_box`
--
ALTER TABLE `bw_box`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bw_boxapplication`
--
ALTER TABLE `bw_boxapplication`
  MODIFY `ba_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bw_clientapplication`
--
ALTER TABLE `bw_clientapplication`
  MODIFY `ca_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bw_module`
--
ALTER TABLE `bw_module`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `bw_purchase`
--
ALTER TABLE `bw_purchase`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bw_room`
--
ALTER TABLE `bw_room`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `bw_roommodule`
--
ALTER TABLE `bw_roommodule`
  MODIFY `rm_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bw_sensorbinary`
--
ALTER TABLE `bw_sensorbinary`
  MODIFY `sb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `bw_sensormultilevel`
--
ALTER TABLE `bw_sensormultilevel`
  MODIFY `sml_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `bw_switch`
--
ALTER TABLE `bw_switch`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `bw_user`
--
ALTER TABLE `bw_user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `bw_userbox`
--
ALTER TABLE `bw_userbox`
  MODIFY `ub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bw_box`
--
ALTER TABLE `bw_box`
  ADD CONSTRAINT `bw_box_ibfk_1` FOREIGN KEY (`b_fk_ba_id`) REFERENCES `bw_boxapplication` (`ba_id`),
  ADD CONSTRAINT `bw_box_ibfk_2` FOREIGN KEY (`b_fk_p_id`) REFERENCES `bw_purchase` (`p_id`);

--
-- Constraints for table `bw_module`
--
ALTER TABLE `bw_module`
  ADD CONSTRAINT `bw_module_ibfk_1` FOREIGN KEY (`m_fk_box_id`) REFERENCES `bw_box` (`b_id`);

--
-- Constraints for table `bw_room`
--
ALTER TABLE `bw_room`
  ADD CONSTRAINT `r_fk_user_id` FOREIGN KEY (`r_fk_user_id`) REFERENCES `bw_user` (`u_id`) ON DELETE CASCADE;

--
-- Constraints for table `bw_roommodule`
--
ALTER TABLE `bw_roommodule`
  ADD CONSTRAINT `rm_fk_module_id` FOREIGN KEY (`rm_fk_module_id`) REFERENCES `bw_module` (`m_moduleid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bw_sensorbinary`
--
ALTER TABLE `bw_sensorbinary`
  ADD CONSTRAINT `sb_fk_module_id` FOREIGN KEY (`sb_fk_module_id`) REFERENCES `bw_module` (`m_moduleid`);

--
-- Constraints for table `bw_sensormultilevel`
--
ALTER TABLE `bw_sensormultilevel`
  ADD CONSTRAINT `sml_fk_module_id` FOREIGN KEY (`sml_fk_module_id`) REFERENCES `bw_module` (`m_moduleid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bw_switch`
--
ALTER TABLE `bw_switch`
  ADD CONSTRAINT `s_fk_module_id` FOREIGN KEY (`s_fk_module_id`) REFERENCES `bw_module` (`m_moduleid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bw_userbox`
--
ALTER TABLE `bw_userbox`
  ADD CONSTRAINT `bw_userbox_ibfk_1` FOREIGN KEY (`ub_fk_user_id`) REFERENCES `bw_user` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bw_userbox_ibfk_2` FOREIGN KEY (`ub_fk_box_id`) REFERENCES `bw_box` (`b_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
