// config/passport.js

// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy  = require('passport-twitter').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

// load up the user model
var User       = require('../app/models/models.js').User;
var usermodel = require('../app/user.js');
// load the auth variables
var configAuth = require('./auth');

module.exports = function(passport) {
    console.log("PASSPORT MODULE LOADED");
    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
	console.log("SERIALIZING USER : ", user);
	done(null, user.u_gid);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
	User.findAll({where: {u_gid: id}}).then(function(rowuser) {
	    console.log("Deserialising user : ", id, " | ", done);
	    done(null, rowuser[0].dataValues);
	}).error(function(erruser)
		 {
		     console.log("error desrializing");
		     done(erruser, null);
		 });
    });

    // code for login (use('local-login', new LocalStategy))
    // code for signup (use('local-signup', new LocalStategy))
    // code for facebook (use('facebook', new FacebookStrategy))
    // code for twitter (use('twitter', new TwitterStrategy))

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

	clientID        : configAuth.googleAuth.clientID,
	clientSecret    : configAuth.googleAuth.clientSecret,
	callbackURL     : configAuth.googleAuth.callbackURL,

    },
				    function(token, refreshToken, profile, done) {
					console.log("IN FUNCTION NEW STRATEGY");
					// make the code asynchronous
					// User.findOne won't fire until we have all our data back from Google
					process.nextTick(function() {
					    console.log("Process Next ticke");
					    // try to find the user based on their google id
					    User.findAll({ where: { u_gid: profile.id }}).then(function(rowuser) {
						if (rowuser.length > 0) {
						    console.log("USER FOUND : ");
						    // if a user is found, log them in
						    return done(null, rowuser[0].dataValues);
						} else {
						    console.log("new user should be created here");
						    usermodel.createuser(null, profile.id, profile.emails[0].value, profile.displayName);
						    /*
						    // if the user isnt in our database, create a new user
						    var newUser          = new User();

						    // set all of the relevant information
						    newUser.google.id    = profile.id;
						    newUser.google.token = token;
						    newUser.google.name  = profile.displayName;
						    newUser.google.email = profile.emails[0].value; // pull the first email

						    // save the user
						    newUser.save(function(err) {
						    if (err)
							throw err;
							return done(null, newUser);
							});
						    */
						}}).error(function(erruser)
							  {
							      console.log("error getting user in db : ", erruser);
							  });
					});
				    }));
};

