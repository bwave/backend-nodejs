Windows Installation (tested with WIN 10 home x64)

Download nodejs installer (not the binary, the installer, and install it) on official website (last version)
Download and install git on official website (last version)
open an cmd with administrator right
type in : 
git config --system core.longpaths true
then clone the repo.
then : npm install


Install last version of mysql, create a database named bwave and import the bwave database backup present in the folder config/database
Credential for sql can be change in those files :
app/database.js
app/models/sequelizeconnexion.js

to run it : 
node server.js (server will run on port 8000)