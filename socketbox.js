//server.js
//var app = require('express')();
//var http = require('http').Server(app);
//var io = require('socket.io')(http);
//var sqlconnexion = require('./database.js');

var Sequelize = require('./app/models/sequelizeconnexion.js').Sequelize;
BoxApplication = require('./app/models/models.js').BoxApplication;
Box = require('./app/models/models.js').Box;
Commands = require('./app/commands.js');
Module = require('./app/modules.js');

//var io = require('./server.js').io;
//var myEmitter = require('./server.js').myEmitter;
module.exports = 
{
	myEmitter: null,
	io:null,
	bo_id:null,
	sockettable: [],
	answerback: function(req, res)
	{
		console.log("answerback is fired,", this.myEmitter);
		module.exports.myEmitter.emit("output", req, res);
	},
	init: function(req, res)
	{
//		console.log("INit : ", this.myEmitter);
this.myEmitter.on("hello", function(req, res)
{
	console.log("In hello EMITTER : ");
	module.exports.answerback(req, res);
});

},
db_addmodule: function(ba_id, moduleid/*, name, description*/)
{
	Module.updatemodule(ba_id, moduleid/*, name, description*/);
}, 
db_deletemodule: function(mdelete)
{
	Module.deletemodule(mdelete);

},
db_savemodulestate: function(ba_id, moduleid, name, id, instance, level, scale)
{
	module.exports.myEmitter.on('test', function()
	{
		console.log("EMITTER FIRED IN DB_SAVEMODULESTATE");
	});
	console.log("info :", name, id, instance, level, scale, moduleid, ba_id)
	if (name == "SensorMultiLevel")
		Commands.saveSensorMultiLevel(ba_id, moduleid, name, id, instance, level, scale);
	else if (name == "SwitchBinary")
		Commands.saveSwitch(ba_id, moduleid, name, id, instance, level, scale);
	else if (name == "SensorBinary")
		Commands.saveSensorBinary(ba_id, moduleid, name, id, instance, level);
},
moduledeleteparse: function(obj, delete_m, ba_id)
{
	moduletodelete = [];
	for (i in obj.modules)
	{
		for (j in obj.modules[i])
		{
//			    for (k in obj.modules[i][j])
//			    {
		/*		console.log("k = ", k);
				console.log("=>", obj.modules[i][j][k]["name"]);
				console.log("=>", obj.modules[i][j][k]["id"]);
				console.log("=>", obj.modules[i][j][k]["instance"]);
				console.log("=>", obj.modules[i][j][k]["level"]);
				console.log("=>", obj.modules[i][j][k]["scale"]);
				*/
		//		test.push([obj.modules[i][j][k]["name"],obj.modules[i][j][k]["id"],obj.modules[i][j][k]["instance"],obj.modules[i][j][k]["level"],obj.modules[i][j][k]["scale"]]);
//			    }
if (typeof(obj.modules[i][j]) == "number")
{
	moduleid = obj.modules[i][j];
	moduletodelete.push(moduleid);
					//for (i in test)
					  //  module.exports.db_savemodulestate(boxid, moduleid, test[i][0], test[i][1], test[i][2], test[i][3], test[i][4], test[i][5]);
					  test = [];
					}
				}
			}
			if (delete_m)
			{
				console.log("deleting old module traces");
				module.exports.db_deletemodule(moduletodelete);
			}
			else
			{
				console.log("Adding new module");
				for (i in moduletodelete)
				{
					console.log("Adding module with id : ", moduletodelete[i])
					module.exports.db_addmodule(ba_id, moduletodelete[i]);
				}
			}
		},
		parsJson: function(obj, boxid)
		{
			test = [];
			moduletodelete = [];
			console.log("deleting old module traces");
			module.exports.db_deletemodule(moduletodelete);
			console.log("Adding new module");
			module.exports.db_addmodule(moduletodelete);
			console.log(obj.modules);
			for (i in obj.modules)
			{
				for (j in obj.modules[i])
				{
					for (k in obj.modules[i][j])
					{
		/*		console.log("k = ", k);
				console.log("=>", obj.modules[i][j][k]["name"]);
				console.log("=>", obj.modules[i][j][k]["id"]);
				console.log("=>", obj.modules[i][j][k]["instance"]);
				console.log("=>", obj.modules[i][j][k]["level"]);
				console.log("=>", obj.modules[i][j][k]["scale"]);
				*/		test.push([obj.modules[i][j][k]["name"],obj.modules[i][j][k]["id"],obj.modules[i][j][k]["instance"],obj.modules[i][j][k]["level"],obj.modules[i][j][k]["scale"]]);
			}
			if (typeof(obj.modules[i][j]) == "number")
			{
				moduleid = obj.modules[i][j];
				moduletodelete.push(moduleid);
				for (i in test)
					module.exports.db_savemodulestate(boxid, moduleid, test[i][0], test[i][1], test[i][2], test[i][3], test[i][4], test[i][5]);
				test = [];
			}
		}
	}
},
updateBoxInfo: function(ip, ba_id, isconnected)
{
	if (ip && ba_id)
		Box.update(
		{
			b_lastip: "192.168.0.99",
//		    	b_lastconnexion: Sequelize.NOW,
b_connected: isconnected
},
{
	where: {b_fk_ba_id: ba_id}
}).then(function(rowbox)
{
	if (rowbox.length > 0)
		console.log("Box successfully updated");
	else
		console.log("No box updated");
}).error(function(errbox)
{
	console.log("Error updating box");
	debug(errbox);
});
},
checkAuthToken: function(login, password,socket)
{
	BoxApplication.findAll({
		where: {ba_identifier: login, ba_password: password}
	}).then(function(rowboxapplication){
		if (rowboxapplication.length > 0)
		{
			bo_id = rowboxapplication[0].ba_id;
			module.exports.updateBoxInfo(socket.handshake.address, rowboxapplication[0].ba_id, true);
			socket.auth = true;
			socket.ba_id = rowboxapplication[0].ba_id;
			console.log("--------------------------------------------------------------------------------- socket : ", socket.ba_id);
			module.exports.sockettable.push(socket);
			console.log("Authenticated socket : ", socket.id);
			socket.emit("ModuleList", function callback(data)
			{
				console.log("emiting modulelist");
				module.exports.moduledeleteparse(data, true, rowboxapplication[0].ba_id);
				module.exports.moduledeleteparse(data, false, rowboxapplication[0].ba_id);
				module.exports.parsJson(data, rowboxapplication[0]["ba_id"]);
				for (i in data)
				{
					console.log("IN For  i : ", i, " = ", data[i]);
					for (j in data[i])
						console.log("j : ", j, " = " , data[i][j]);
				}
				console.log("data : ", data);
			});
			return true;
		}
	}).error(function (errboxapplication)
	{
		console.log("Error login box");
		debug(errboxapplication);
		return false;
	});
},

};
