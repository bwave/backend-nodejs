//server.js
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
//var sqlconnexion = require('./database.js');

var Sequelize = require('../app/models/sequelizeconnexion.js').Sequelize;
BoxApplication = require('../app/models/models.js').BoxApplication;
Box = require('../app/models/models.js').Box;
Commands = require('../app/commands.js');
Module = require('../app/modules.js');

function db_addmodule(ba_id, moduleid, name, description)
{
	Module.updatemodule(ba_id, moduleid, name, description);
	/*

    var query = sqlconnection.query("INSERT INTO `bw_module` (`m_moduleid`, `m_modulename`, `m_description`, `m_fk_box_id`) \
    	VALUES (?,?,?,(SELECT b_id FROM bw_box WHERE b_fk_ba_id = "+ba_id+")) ON DUPLICATE KEY UPDATE `m_modulename` = ?, `m_description` = ?", 
    	[moduleid, name, description, name, description], function (err, rows)
				    {
					if (err)
					    console.log(err);
					else
					{
					    console.log(query.sql);
					    console.log("module successfully added");
					}
				    }
				   );
    */
}
function db_deletemodule(mdelete)
{
	Module.deletemodule(mdelete);
	/*
	data = "(";
    for (i in mdelete)
    {
	data = data + mdelete[i];
	if (i < (mdelete.length - 1))
	{
	    data = data + ",";
	    console.log("i : ", i, "length", mdelete.length);
	}
    }    
    data = data + ")";
    //console.log(data);
    var query = sqlconnection.query("DELETE FROM `bw_module` WHERE `m_moduleid` NOT IN " + data, function(err, rows)
				    {
					if (err)
					    console.log(err);
					else
					    console.log("Module trace successfully deleted");
				    });
*/
}

function db_savemodulestate(ba_id, moduleid, name, id, instance, level, scale)
{
    console.log("info :", name, id, instance, level, scale, moduleid)
    if (name == "SensorMultiLevel")
    	Commands.saveSensorMultiLevel(ba_id, moduleid, name, id, instance, level, scale);
    else if (name == "SwitchBinary")
		Commands.saveSwitch(ba_id, moduleid, name, id, instance, level, scale);
    /*
    var module_id = sqlconnection.query("SELECT m_moduleid FROM bw_module inner join bw_box on bw_box.b_fk_ba_id = ? WHERE m_moduleid = ?", [ba_id, moduleid],
				       function(err, rows)
				       {
					   if (err)
					       console.log("Erreur get module_id");
					   else if (rows <= 0)
					       db_addmodule(ba_id, moduleid, name, "no description");
					   else
					   {
					       console.log("-----------------------------------------------------------------");
					       console.log(rows);
					       if (name == "SensorMultiLevel")
					       {
						   var query = sqlconnection.query("INSERT INTO `bw_sensormultilevel` (`sml_smlid`, `sml_level`, `sml_scale`, `sml_instance`, `sml_fk_module_id`) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE `sml_smlid` = ?, sml_level = ?, sml_scale = ?, sml_instance = ?, sml_fk_module_id = ?",
										   [id, level, scale, instance, rows[0]["m_moduleid"], id, level, scale, instance, rows[0]["m_moduleid"]], function(err, results)
										   {
										       console.log(query.sql);
										       if (err)
											   console.log("Error in database : ", err);
										       else
											   console.log("bw_sensormultilevel request successfull");
										   });
					       }
					       else if (name == "SwitchBinary")
					       {
						   var query = sqlconnection.query("INSERT INTO `bw_switch` (`s_level`, `s_instance`, `s_fk_module_id`) VALUES (?,?,?) ON DUPLICATE KEY UPDATE s_level = ?, s_instance = ?, s_fk_module_id = ?",
										   [level, instance, rows[0]["m_moduleid"], level, instance, rows[0]["m_moduleid"]], function (err, results)
										   {
										       console.log(query.sql);
										       if (err)
											   console.log("Error in database : ", err);
										       else
											   console.log("bw_sitch request successfull");
										   })
					       }
					   }
				       });  
				       */ 
}

function parsJson(obj, boxid)
{
    test = [];
    moduletodelete = [];
    console.log(obj.modules);
    for (i in obj.modules)
    {
	for (j in obj.modules[i])
	{
	    for (k in obj.modules[i][j])
	    {
/*		console.log("k = ", k);
		console.log("=>", obj.modules[i][j][k]["name"]);
		console.log("=>", obj.modules[i][j][k]["id"]);
		console.log("=>", obj.modules[i][j][k]["instance"]);
		console.log("=>", obj.modules[i][j][k]["level"]);
		console.log("=>", obj.modules[i][j][k]["scale"]);
*/		test.push([obj.modules[i][j][k]["name"],obj.modules[i][j][k]["id"],obj.modules[i][j][k]["instance"],obj.modules[i][j][k]["level"],obj.modules[i][j][k]["scale"]]);
	    }
		//		console.log("obj : ", obj.modules[i][j][k]);
	    if (typeof(obj.modules[i][j]) == "number")
	    {
		moduleid = obj.modules[i][j];
		moduletodelete.push(moduleid);
		for (i in test)
		    db_savemodulestate(boxid, moduleid, test[i][0], test[i][1], test[i][2], test[i][3], test[i][4], test[i][5]);
		test = [];
	    }
	}
    }
    console.log("deleting old module traces");
    db_deletemodule(moduletodelete);
    /*    for (var key1 in obj)
		{
		    for (var key2 in obj[key1])
		    {
			for (var key3 in obj[key1][key2])
			{
			    if (typeof(obj[key1][key2][key3])  == "number")
				console.log("ID : obj[key1][key2][key3] : ", obj[key1][key2][key3]);//, obj[key1][key2][key3]);
			    for (var key4 in obj[key1][key2][key3])
			    {				
				console.log("++++++++++++", obj[key1][key2][key3][key4]["name"]);
				console.log("++++++++++++", obj[key1][key2][key3][key4]["id"]);
				console.log("++++++++++++", obj[key1][key2][key3][key4]["instance"]);
				console.log("++++++++++++", obj[key1][key2][key3][key4]["level"]);
				console.log("++++++++++++", obj[key1][key2][key3][key4]["scale"]);
				console.log(boxid, obj[key1][key2][key3][key4]["name"],obj[key1][key2][key3][key4]["id"],
					    obj[key1][key2][key3][key4]["instance"], obj[key1][key2][key3][key4]["level"],
					    obj[key1][key2][key3][key4]["scale"], obj[key1][key2][key3]);
	//			db_savemodulestate(boxid, obj[key1][key2][key3][key4]["name"],obj[key1][key2][key3][key4]["id"],
	//					   obj[key1][key2][key3][key4]["instance"], obj[key1][key2][key3][key4]["level"],
	//					   obj[key1][key2][key3][key4]["scale"]);
		   
			    }
			}
		    }
		}
*/
}

sqlconnection.connect(function(err) {
    if (err) {
	console.error('error connecting: ' + err.stack);
	return;
    }
    console.log("Connected to database");
});
var sockettable=[];
function updateBoxInfo(ip, ba_id, isconnected)
{
	if (ip && ba_id)
	    Box.update(
	    {
	    	b_lastip: ip,
	    	b_lastconnexion: Sequelize.NOW,
	    	b_connected: isconnected
	    },
	    {
	    	where: {b_fk_ba_id: ba_id}
	    }).then(function(rowbox)
	    {
	    	if (rowbox.length > 0)
	    		console.log("Box successfully updated");
	    	else
	    		console.log("No box updated");
	    }).error(function(errbox)
	    {
	    	console.log("Error updating box");
	    	debug(errbox);
	    });
 //   query = ;
 //   console.log(query);
    /*
    if (ip && ba_id)
	q = sqlconnection.query("UPDATE `bw_box` SET `b_lastip`= ?,`b_connected`= ?,`b_lastconnexion`= now() WHERE `b_fk_ba_id` = ?",[ip, isconnected, ba_id], function(err, rows)
				{
				    console.log("Updating box with query : ", q.sql);
				    // si box doesn't exist, no error is returned
				    if (err)
					console.log("error update box");
				    else
				   console.log("box successfully updated");
				});
				*/
}
var bo_id;
function checkAuthToken(login, password,socket)
{
	BoxApplication.findAll({
		where: {ba_identifier: login, ba_password: password}
	}).then(function(rowboxapplication){
		if (rowboxapplication.length > 0)
		{
			bo_id = rowboxapplication[0].ba_id;
			updateBoxInfo(socket.handshake.address, rowboxapplication[0].ba_id, true);
			socket.auth = true;
			sockettable.push(login, socket);
			console.log("Authenticated socket ", socket.id);
			socket.emit("ModuleList", function callback(data)
						    {
							console.log("emiting modulelist");
							parsJson(data, rowboxapplication[0]["ba_id"]);
							for (i in data)
							{
							    console.log("IN For  i : ", i, " = ", data[i]);
							    for (j in data[i])
								console.log("j : ", j, " = " , data[i][j]);
							}
							console.log("data : ", data);
						    });
			return true;
		}
	}).error(function (errboxapplication)
	{
		console.log("Error login box");
		debug(errboxapplication);
		return false;
	});/*
    query = "SELECT * FROM `bw_boxapplication` WHERE `ba_identifier`= '" + login + "' AND `ba_password` = '" + password + "'";
    q = sqlconnection.query(query, function (err, rows)
			    {
				if (err)
				{
				    console.log("ERROR IN DATABASE : ", err);
				    return false;
				}
				else
				{
				    if (rows.length > 0)
				    {
					bo_id = rows[0].ba_id;
					console.log("rows : ", rows);
					socket.auth = true;
					sockettable.push(login, socket);
//					console.log("-----------------------------" + sockettable[0]);
					socket.emit("ModuleList", function callback(data)
						    {
							console.log("emiting modulelist");
							parsJson(data, rows[0]["ba_id"]);
							for (i in data)
							{
							    console.log("IN For  i : ", i, " = ", data[i]);
							    for (j in data[i])
								console.log("j : ", j, " = " , data[i][j]);
							}
							console.log("data : ", data);
						    });
					
					return true;
				    }
				    return false;
				}
			    }
			   );*/
}

io.on('connection', function (socket)
      {
	  socket.auth = false;
	  console.log('connection');
	  socket.on('authenticate', function(data)
		    {
			//check the auth data sent by the client
			checkAuthToken(data.login, data.password, socket);
			console.log(sockettable);
			socket.on('CH01', function (from, msg)
				  {
				      console.log('MSG', from, ' saying ', msg);
				  });
			socket.once('disconnect',function()
				     {
					updateBoxInfo(socket.handshake.address, bo_id, false);
				    });
			setTimeout(function(){
			    //If the socket didn't authenticate, disconnect it
			    if (!socket.auth) {
				console.log("Disconnecting socket ", socket.id);
				socket.disconnect('unauthorized');
			    }
			}, 1000);
		    });
      });

http.listen(3001, function () {
    console.log('listening on *:3001');
});
