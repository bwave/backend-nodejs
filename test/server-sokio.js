
//server.js
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
//var sqlconnexion = require('./database.js');

var mysql = require('mysql');

var sqlconnection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'bwave',
    database : 'bwave'
});

sqlconnection.connect(function(err) {
    if (err) {
	console.error('error connecting: ' + err.stack);
	return;
    }
    console.log("Connected to database");
});
var sockettable=[];
function updateBoxInfo(ip, ba_id)
{
    query = "UPDATE `bw_box` SET `b_lastip`= '" + ip + "',`b_connected`= " + true + ",`b_lastconnexion`= now() WHERE `b_fk_ba_id` = " + ba_id;
    console.log(query);
    q = sqlconnection.query(query, function(err, rows)
			    {
				// si box doesn't exist, no error is returned
			       if (err)
				   console.log("error update box");
			       else
				   console.log("box successfully updated");
			   });
}
function checkAuthToken(login, password,socket)
{
    query = "SELECT * FROM `bw_boxapplication` WHERE `ba_identifier`= '" + login + "' AND `ba_password` = '" + password + "'";
    q = sqlconnection.query(query, function (err, rows)
			   {
			       if (err)
			       {
				   console.log("ERROR IN DATABASE : ", err);
				   return false;
			       }
			       else
			       {
				   if (rows.length > 0)
				   {
				       updateBoxInfo(socket.handshake.address, rows[0].ba_id);
				       // UPDATE BW_BOX INFORMATION
				       console.log("rows : ", rows);
				       console.log("Authenticated socket ", socket.id);
				       socket.auth = true;
				       sockettable.push(login, socket);
				       console.log("-----------------------------" + sockettable[0]);
				       console.log(socket.handshake.address);
				       socket.emit("ModuleList", function callback(data)
						   {
						       // UPDATE BW_MODULE
						       console.log("data : ", data);
						   });
				       return true;
				   }
				   return false;
			       }
			   }
			   );
}

io.on('connection', function (socket)
      {
	  socket.auth = false;
	  console.log('connection');
	  socket.on('authenticate', function(data)
		    {
			//check the auth data sent by the client
			checkAuthToken(data.login, data.password, socket);
			console.log(sockettable);
			socket.on('CH01', function (from, msg)
				  {
				      console.log('MSG', from, ' saying ', msg);
				  });
			setTimeout(function(){
			    //If the socket didn't authenticate, disconnect it
			    if (!socket.auth) {
				console.log("Disconnecting socket ", socket.id);
				socket.disconnect('unauthorized');
			    }
			}, 1000);
		    });
      });

http.listen(3000, function () {
    console.log('listening on *:3000');
});
/*
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')()//(http);
app.get('/', function(req, res){
    res.sendfile('index.html');
});
io.on('connection', function(socket){
    console.log('a user connected');
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});
*/
