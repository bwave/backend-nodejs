var mysql = require('mysql');

var sqlconnection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'toor',
    database : 'bwave'
});

sqlconnection.connect(function(err) {
    if (err) {
	console.error('error connecting: ' + err.stack);
	return;
    }
    console.log("Connected to database");
});
module.export = sqlconnection;
